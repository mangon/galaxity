import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openmarkov.io.probmodel.PGMXReader; // Read .PGMX
import org.openmarkov.core.model.network.ProbNet; // Network

public class IDTest {
	public static void test() throws IOException, Exception {
		String filename = "intention_ID.pgmx";
		FileInputStream file  = new FileInputStream(new File(filename));
		PGMXReader reader = new PGMXReader();
		ProbNet model = reader.loadProbNet(file, filename).getProbNet();
		System.out.println(model);
	}

	public static void main(String[] args) {
		try {
			test();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();			
		}
	} 
}