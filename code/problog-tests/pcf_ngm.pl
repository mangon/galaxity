:- use_module( library(problog)).

%
%	Perception Correction Function for the Noisy Gold Miners Scenario
%

%
%	We want to estimate the content of each cell.
%	
%	Let cell(N,X) be the predicate that describes the content of the cell number N
%
%	We know that the current content of a cell depends, probabilistically from:
%		- the current action
%		- the previous content of that cell neighbours
%		- the current scanner report of that cell
%
%	Let scanner(N,X) be the reported content of cell number N 
%	and previous(N,X) be the previous content of cell number N
%
%	Actions are:
%
%		skip, pick, drop, up, down, left, right
%
action(skip).
action(pick).
action(drop).
action(up).
action(down).
action(left).
action(right).
%
%	Cell locations are 0, ..., 8 in a grid:
%
%		0 1 2
%		3 4 5
%		6 7 8
%
%	neighbours(C,N) :- X is neighbour of C iff X in N
%
neighbours(0,[0,1,3]).
neighbours(1,[0,1,2,4]).
neighbours(2,[1,2,5]).
neighbours(3,[0,3,4,6]).
neighbours(4,[1,3,4,5,7]).
neighbours(5,[2,4,5,8]).
neighbours(6,[3,6,7]).
neighbours(7,[4,6,7,8]).
neighbours(8,[5,7,8]).
%
loc(X) :- neighbours(X,_).
%
%	Cell values are:
%
%		empty, obstacle, gold, miner
val(empty).
val(obstacle).
val(gold).
val(miner).
%
%	Motions move the sensor grid outside the previous scan-range.
%
outside(up, 0).
outside(up, 1).
outside(up, 2).
outside(down, 6).
outside(down, 7).
outside(down, 8).
outside(left, 0).
outside(left, 3).
outside(left, 6).
outside(right, 2).
outside(right, 5).
outside(right, 8).
motion(I,I-3, up). 
motion(I,I+3, down).
motion(I,I-1, left).
motion(I,I+1, right).
moves(up).
moves(down).
moves(left).
moves(right).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Sensor model
%
%	P( scanner(N,X) | cell(N,X1) )
%
%	scanner( Location, SensorValue, CellValue)
%
sensor_noise(0.001).
0.999 :: sensor(val(X), val(X)).
<<<<<<< HEAD
0.00033 :: sensor(val(X), val(Y)) <-- \+(X = Y).
=======
0.00033 :: sensor(val(X), val(Y)) <-- X \== Y.
>>>>>>> FETCH_HEAD

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Transition model
%
%	P( cell(N,X) | previous(N1, X1), ..., previous(Nk, Xn), Action)
%
%	transition( cell(N,X), previous(N1,X1), ..., previous(Nk, Xk), Action)
%
%----------------------------------------------------------
%
%	Action: skip
%
1.0 :: transition( cell(N,X), previous(N,X), skip ).
%
%	Action: pick
%
1.0 :: transition( cell( loc(I), X), previous(loc(I),X), pick) <-- I \= 4.
1.0 :: transition( cell( loc(4), val(empty)), previous(loc(4), val(gold)), pick).
1.0 :: transition( cell( loc(4), val(X)), previous(loc(4), val(X)), pick) <-- X \= gold.
%
%	Action: drop	
%
1.0 :: transition( cell( loc(I), X), previous(loc(I),X), drop) <-- I \= 4.
1.0 :: transition( cell( loc(4), val(gold)), previous(loc(4), val(empty)), drop).
1.0 :: transition( cell( loc(4), val(X)), previous(loc(4), val(X)), drop) <-- X \= empty.
%
%	Action: up, down, left, right
%
0.25 :: transition( cell(loc(I), val(X)), _, Action) <--
	moves(Action),
	outside(Action, I).
1.00 :: transition( cell(loc(I), Val), previous(loc(J), Val), Action) <--
	moves(Action),
	\+(outside(Action, I)),
	motion(I, J, Action).