#!/usr/local/bin/yap -L --
#.

:- use_module(library(lists), [member/2]).

valid_position(X,Y) :-
	integer(X),
	integer(Y),
	-2 =< X,
	X =< 2,
	-2 =< Y,
	Y =< 2.

move(left, -1, 0).
move(right, 1, 0).
move(up, 0, -1).
move(down, 0, 1).

walk_step(cell(X,Y), Dir, cell(X1,Y1)) :-
	valid_position(X,Y),
	move(Dir,DX,DY),
	X1 is X + DX,
	Y1 is Y + DY,
	valid_position(X1,Y1).


walk(X, [], X, []).
walk(cell(X,Y), [Dir | W], cell(X1,Y1), [cell(X1,Y1) | V]) :-
	move(Dir, _, _),
	walk_step( cell(X,Y), Dir, cell(X2,Y2) ),
	walk(cell(X2,Y2), W, cell(X1,Y1), V),
	\+member(cell(X1,Y1), V).

:- 
	Source = cell(0,0),
	Target = cell(1,1),
	write('Source: '), write(Source), nl,
	write('Target: '), write(Target), nl,
	/*debug, debugging, spy(walk),*/
	walk(Source, [left, up,right], X, Visited),
	write('Path: '), write(Visited), nl.

/*
:- findall([X,Y], cell_pos(X,Y), L), write(L), nl.
*/