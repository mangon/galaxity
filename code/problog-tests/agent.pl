% Problog library
:- use_module(library(problog)).

% percept(sensor_id, value, timestamp) and log(data) are 
% dynamic predicates. Kind of agent of db perceptions
:- dynamic percept/3, log/1.

% environment interaction
% perception(S, V, T) percepts sensor S with value V at timestamp T
% adds this to the database, logging previous sensors value (if exists)
% example 
% perception(s1, 90, 1).

perception(Sensor, Value, Time) :-
	(percept(Sensor, Previous_Value, Previous_Time) -> 
		(retract(percept(Sensor, _, _)), 
		asserta(log(percept(Sensor, Previous_Value, Previous_Time)))
		); true
	),
	asserta(percept(Sensor, Value, Time)),
	
	(belief(Prob, error(Sensor)) -> 
	generate_belief(Prob, Sensor, Value);
	generate_belief(1, Sensor, Value)).

generate_belief(Prob, Sensor, Value) :-
	Prob :: sensor(Sensor, Value),
	!.

generate_belief(Prob, Sensor, Value) :-
	(P1 :: sensor(Sensor, V1) ->
	retract(P1 :: sensor(Sensor, V1));
	true
	),
	asserta(Prob :: sensor(Sensor, Value)).

% belief(Probability, Fact?)
belief(0.1, error(s1)).


% plans

% goals   



% from sensor(s1, 90) '@' 0. and belief :: 0.1 :: error(s1).
% infer belief ::  0.1 :: sensor(s1, 90) '@' 0.


% Temporal Algebra (time points)


:- op(1200, xfy, '@').

