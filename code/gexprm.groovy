import edu.umass.cs.mallet.grmm.inference.Inferencer
import edu.umass.cs.mallet.grmm.inference.JunctionTreeInferencer
import edu.umass.cs.mallet.grmm.types.*

import java.util.Random


Variable[] allVars = [
      new Variable (2),
      new Variable (2),
      new Variable (2),
      new Variable (2)
	  ]

numVars = allVars.length
		  	  
mdl = new FactorGraph( allVars )
r = new Random(42)		  
		  
(0 ..< numVars).each { i ->
	d = new double[numVars]
	(0 ..< numVars).each { j ->
		d[j] = Math.abs( r.nextDouble() )
	}
	
	v1 = allVars[i]
	v2 = allVars[(i + 1) % numVars]
	mdl.addFactor( v1, v2, d)
}
	
inf = new JunctionTreeInferencer ()
inf.computeMarginals (mdl)		
	
(0 ..< numVars).each { i->
	v = allVars[i]
	marg = inf.lookupMarginal(v)
	marg.assignmentIterator().collect().each {
		println "${v} ${it}"
	}
}