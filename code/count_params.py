def count(c_range, num_parents):
	return c_range * (c_range**num_parents - 1)

def sub_cases(num_parents):
	return [3,4,2][num_parents -2]

s = sum([count(4,i)*sub_cases(i) for i in range(2,5) ])
print(s)