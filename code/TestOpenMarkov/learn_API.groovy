import java.io.InputStream
import java.util.ArrayList
import java.util.HashMap

import org.openmarkov.core.exception.InvalidStateException
import org.openmarkov.core.inference.InferenceAlgorithm
import org.openmarkov.core.model.network.EvidenceCase
import org.openmarkov.core.model.network.Finding
import org.openmarkov.core.model.network.ProbNet
import org.openmarkov.core.model.network.Util
import org.openmarkov.core.model.network.Variable
import org.openmarkov.core.model.network.potential.TablePotential
import org.openmarkov.inference.variableElimination.VariableElimination
import org.openmarkov.io.probmodel.PGMXReader

evidence = new EvidenceCase()

bayesNetworkName = "/Users/fc/Documents/prog/src/Galaxity/code/TestOpenMarkov/BN-two-diseases.pgmx"

println bayesNetworkName
file = getClass().getClassLoader().getResourceAsStream(bayesNetworkName)
println file
pgmxReader = new PGMXReader()
probNet = pgmxReader.loadProbNet(file, bayesNetworkName).getProbNet()
evidence.addFinding(probNet, "Symptom", "present")




variableElimination = new VariableElimination(probNet)

evidence.addFinding(probNet, "Symptom", "present")
variableElimination.setPreResolutionEvidence(evidence)

disease1 = probNet.getVariable("Disease 1")
disease2 = probNet.getVariable("Disease 2")
variablesOfInterest = new ArrayList<Variable>()
variablesOfInterest.add(disease1)
variablesOfInterest.add(disease2)

posteriorProbabilities = variableElimination.getProbsAndUtilities(variablesOfInterest)
