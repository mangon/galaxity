* The example `ExampleAPI` works after...
    1. Correcting the `javac.args` and `jar.args` in the `openmarkov` distribution (correcting the windows centric path notation)
    2. Setting the `CLASSPATH` to the `jar` libraries and compiled classes (the script `set-openmarkov-classpath` does that assuming that `$OPENMARKOV` points to the OpenMarkov directory)
    
* The development of OpenMarkov is **active** with several the bitbucket repositories