package ints

import (
	"math/rand"
	"testing"
)

func TestConstructors(t *testing.T) {
	a := New([]int{2, 3, 4})
	b := Create(2, 3, 4)
	if !a.Equals(b) {
		t.Errorf("New/Create/Equals: expecting %v got %v", a, b)
	}
}

func TestBasicAccess(t *testing.T) {
	// Get
	arr := []int{2, 4, 5, 1, 8, 1, 7, 1, 9, 6}
	a := New(arr)
	for i := 0; i < a.Size(); i++ {
		if a.Get(i) != arr[i] {
			t.Errorf("Get: expecting %d got %d", arr[i], a.Get(3))
		}
	}

	a.Set(2, 4)
	if a.Get(2) != 4 {
		t.Errorf("Set: expecting 4 got %d", a.Get(2))
	}
}

func TestCodecOps(t *testing.T) {
	a := New([]int{3, 2, 4, 2})
	r := rand.New(rand.NewSource(1234))
	n := a.Reduce(func(x, y int) int {
		return x * y
	}, 1)
	for i := 0; i < 1000; i++ {
		index := r.Intn(n)
		x := a.Dec(index)
		j := a.Enc(x)
		if j != index {
			t.Errorf("Codec: expecting %d got %d", index, j)
		}
	}
}

func TestArrayOps(t *testing.T) {
	a := New([]int{0, 1, 2, 3, 4})
	b := New([]int{5, 6, 7, 8, 9})
	c := a.Append(b)
	for i, ci := range c.Values() {
		if i != ci {
			t.Errorf("Append: expecting %d got %d in %v", i, ci, c)
		}
	}

	a = New([]int{2, 1, 9, 3, 8, 0, 4, 7, 5, 6})
	c = a.Sorted()
	for i, ci := range c.Values() {
		if i != ci {
			t.Errorf("Append: expecting %d got %d in %v", i, ci, c)
		}
	}

	a = New([]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})
	c = a.Reversed()
	for i, ci := range c.Values() {
		if i != ci {
			t.Errorf("Reversed: expecting %d got %d in %v", i, ci, c)
		}
	}
}

func TestIndexOps(t *testing.T) {
	a := New([]int{0, 1, 2, 3, 4, 5})
	for i, ai := range a.Values() {
		as := a.SearchOne(ai)
		if i != as {
			t.Errorf("SearchOne: expecting %d got %d in %v", i, as, a)
		}
	}

	i := Create(6, -1, 10, 3, 3, 1)
	b := a.Compose(i)
	c := Create(3, 3, 1)
	for i, bi := range b.Values() {
		if bi != c.Get(i) {
			t.Errorf("Compose: expecting %d got %d in %v", c.Get(i), bi, b)
		}
	}

	ii := a.Search(b)
	if !b.Equals(ii) {
		t.Errorf("Compose: expecting %v got %v in %v", a, ii, b)
	}

}
