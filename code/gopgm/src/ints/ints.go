// Package ints provides operations on lists of integers, required to perform basic management fo PGMs.
package ints

import (
	"bytes"
	"fmt"
	"sort"
)

type Ints struct {
	arr []int
}

// New creates an Ints from a slice of ints. It is the default constructor.
func New(a []int) Ints {
	return Ints{
		a,
	}
}

func Create(a ...int) Ints {
	return New(a)
}

// Project selects the i-th element of a collection of Ints.
func Project(a []Ints, i int) Ints {
	x := make([]int, len(a))
	for k, ak := range a {
		x[k] = ak.Get(i)
	}
	return New(x)
}

func (a Ints) Get(i int) int {
	return a.arr[i]
}

func (a Ints) Set(i int, x int) {
	a.arr[i] = x
}

func (a Ints) Size() int {
	return len(a.arr)
}

func (a Ints) Values() []int {
	return a.arr
}

func (a Ints) Equals(b Ints) bool {
	if len(a.arr) != len(b.arr) {
		return false
	}
	for i := range a.arr {
		if a.arr[i] != b.arr[i] {
			return false
		}
	}
	return true
}

func (a Ints) Format(format string) string {
	var b bytes.Buffer
	for _, x := range a.arr {
		b.WriteString(fmt.Sprintf(format, x))
	}
	return b.String()
}

func (a Ints) String() string {
	return fmt.Sprintf("%v", a.arr)
}

func (a Ints) Dec(i int) Ints {
	n := a.Size()
	c := make([]int, n)
	x := i
	for k, ak := range a.arr {
		c[k] = x % ak
		x = x / ak
	}
	return New(c)
}

func (a Ints) Enc(x Ints) int {
	n := a.Size() - 1
	i := x.Get(n)
	for k := n - 1; k >= 0; k-- {
		i = a.Get(k)*i + x.Get(k)
	}
	return i
}

func (a Ints) Map(f func(int) int) Ints {
	b := make([]int, a.Size())
	for i, x := range a.arr {
		b[i] = f(x)
	}
	return New(b)
}

func (a Ints) Reduce(f func(int, int) int, f0 int) int {
	y := f0
	for _, x := range a.arr {
		y = f(x, y)
	}
	return y
}

func (a Ints) Filter(f func(int) bool) Ints {
	b := make([]int, a.Size())
	k := 0
	for _, x := range a.arr {
		if f(x) {
			b[k] = x
			k++
		}
	}
	return New(b[:k])
}

func (a Ints) Append(b Ints) Ints {
	c := make([]int, 0)
	c = append(c, a.arr...)
	c = append(c, b.arr...)
	return New(c)
}

func (a Ints) Sorted() Ints {
	var b []int
	copy(b, a.arr)
	sort.Ints(b)
	return New(b)
}

func (a Ints) Reversed() Ints {
	n := len(a.arr)
	b := make([]int, n)
	for i, ai := range a.arr {
		b[n-i-1] = ai
	}
	return New(b)
}

func (a Ints) SearchOne(x int) int {
	for i, ai := range a.arr {
		if ai == x {
			return i
		}
	}
	return -1
}

func (a Ints) Search(x Ints) Ints {
	b := make([]int, x.Size())
	for i, xi := range x.Values() {
		b[i] = a.SearchOne(xi)
	}
	return New(b)
}

func (a Ints) Compose(b Ints) Ints {
	c := make([]int, b.Size())
	k := 0
	for _, bi := range b.Values() {
		if 0 <= bi && bi < a.Size() {
			c[k] = a.arr[bi]
			k++
		}
	}
	return New(c[:k])
}
