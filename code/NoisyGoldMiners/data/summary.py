import os
import fileinput

'''
This program summarizes data in files "short-result_n<NOISE>_a<AGENT>_r<RUN>.txt"
Assumes that these files have two lines with format

INFO: Cycle 700 finished in 12 ms, mean is <MEANRUN>.
WARNING: ** Finished at the maximal number of steps! Red x Blue = <REDS> x <BLUES>

The result is a dataset that aggregates runs:

	data[<NOISE>][<AGENT>] = [ [<MEANRUN>, <REDS>, <BLUES>], ]


'''
data = dict()
for x in os.listdir("."):
	if x.startswith("short"):
		segments = x.split("_")
		noise_id = segments[1]
		agent_id = segments[2]
		run_id = segments[3].split(".")[0]
		if not noise_id in data.keys():
			data[noise_id] = dict()
		if not agent_id in data[noise_id].keys():
			data[noise_id][agent_id] = list()
		for line in fileinput.input(x):
			if line.startswith("INFO"):
				run_mean = int(line.split("mean is ")[-1].split(".")[0])
			if line.startswith("WARNING")
			else:
				counts = map(int, line.split("=")[1].replace(" ","").replace("\n","").split("x"))
		data[noise_id][agent_id].append([run_mean] + counts)
for noise in data.keys():
	print "Noise: ",noise
	for agent in data[noise]:
		mean_run = 0.0
		mean_red = 0.0
		mean_blue = 0.0
		n = len(data[noise][agent])
		for datapoint in data[noise][agent]:
			mean_run += datapoint[0]
			mean_red += datapoint[1]
			mean_blue += datapoint[2]

		mean_run /= n
		mean_red /= n
		mean_blue /= n
		print "\tAgent: %s ==> run: %4.2f reds: %4.2f blues: %4.2f"%(agent, mean_run, mean_red, mean_blue)


