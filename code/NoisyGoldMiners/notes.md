# Notes

## What is, and what for and how it is used:

* Infrastructure
	* can be either "Centralized" or "Saci"
	* defines the underlaying that run the multi-agent system

* Environment
	* define the "getPercepts(agName)" and "executeAction(agName, act)"
* Control
* Agent architecture class
* Belief base class
* Agent class