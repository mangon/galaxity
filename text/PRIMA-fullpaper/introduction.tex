%!TEX root = ./PBRFASL.tex
\section{Introduction and Motivation}
\noindent
Agent autonomy is a key objective in \ac{AI}. Complex and dynamic environments, like the physical world where robots must delve, impose a degree of uncertainty that challenges the vocation of symbolic processing. But while a probabilistic approach --- currently expressed in \ac{ML} and \acp{PGM} \cite{koller2009probabilistic}--- is required for certain aspects of autonomy, a great deal of agent programming is better handled by declarative programming (\emph{e.g.} \PROLOG) and more specifically, \ac{BDI} architectures for autonomous agents, part of symbolic \ac{AI}.%\AUTHNOTE{\textbf{language}~ are \emph{symbolic}, \emph{logic} and \emph{declarative} being properly used?}


Although the symbolic and probabilistic areas of \ac{AI} correspond, in fact, to different and often antagonist cultures and perspectives, they are not necessarily incompatible.
%
Bridges between them are being built based on distribution semantics \cite{sato1995statistical} or markov logic \cite{domingos2006unifying}. From that common ground there are two possible paths towards the interplay of symbolic and probabilistic \ac{AI}: the extension of \acp{PGM} with lo\-gi\-cal and relational  representations (done by  \ac{SRL}) \cite{sato2011general,sato2007inside} and the extension of logic programming languages with probability, in \ac{PLP} \cite{Fierens:2013fk,fierens2012inference,gutmann2011learning}.

%
From the point of view of programming autonomous agents the sym\-bo\-lic \emph{vs.} pro\-ba\-bi\-lis\-tic division essentially still persists: symbolic architectures, such as \ac{BDI}, describe agent behavior on the basis of metaphors (\emph{e.g.} goals, beliefs, plans) drawn from human behavior while the principle of \ac{MEU} is included, as influence diagrams, in probabilistic  \ac{AI} but there is only seminal work blurring that division.

%\begin{figure*}[t]
%\begin{center}
%\begin{tikzpicture}
%\node at (0,2) (ENV1) {Percepts};
%
%\node[draw] at (1.5,2) (BRF) {BRF};
%
%\node[draw, rounded corners] at (3.5,3) (BB) {Beliefs};
%\node[draw, rounded corners] at (3.5,1) (EV) {Events};
%\node[draw, rounded corners] at (3.5,2) (PL) {Plans Library};
%
%\node[draw] at (6,3) (GO) {Generate Options};
%\node[draw,fill=gray!50!white] at (6,1) (SE) {\Sel{E}};
%
%\node[draw,fill=gray!50!white] at (8,3) (SO) {\Sel{O}};
%\node[draw, rounded corners] at (8,2) (IQ) {Intentions};
%\node[draw,fill=gray!50!white] at (8,1) (SI) {\Sel{I}};
%
%\node at (10,2) (ENV2) {Action};
%\path[->, every edge/.style={cond}]
%	(ENV1) edge (BRF)
%%	(BRF) edge (BB)
%%	(BB) edge (BRF)
%%	(BRF) edge (EV)
%	(EV) edge (SE)
%%	(PL) edge (GO)
%	(SE) edge (GO)
%	(BB) edge (GO)
%	(GO) edge (SO)
%	(SO) edge (IQ)
%	(IQ) edge (SI)
%%	(SI) edge (ENV2)
%;
%\draw[cond,->,rounded corners=3pt] (PL) -| (GO);
%\draw[cond,->,rounded corners=3pt] (BRF) |- (EV);
%\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
%\draw[cond,->,rounded corners=3pt] (SI) -| (ENV2);
%\end{tikzpicture}
%\end{center}
%%
%\caption{The \JASON\ deliberation process very resumed, with selection functions highlighted. The \ac{ASL} only specifies the signature of the \Sel{E}, \Sel{O} and \Sel{I} functions omitting any conditions, besides the type, on the output.}
%%
%\label{fig:jason.deliberation}
%\end{figure*}{}
%
Concerning agents programming \JASON\ \cite{bordini2007programming} is a popular \acf{ASL} \cite{rao1996agentspeak} interpreter and framework, triggering a considerable amount of research (\emph{e.g.} \cite{bordini2010semantics,bordini2006bdi}). The \ac{BDI} architecture in general, including \ac{ASL} and \JASON\ in particular, outline a set of symbolic data structures and processes with more or less detailed semantics. 

However we can see \JASON\ agents in trouble when their environment  becomes stochastic. This intuitive assertion is supported by a simple experiment plotted in Figure \ref{fig:experiment1.results}: the \GM\ is a virtual scenario used in the 2006 Multi-Agent Programming Contest \cite{behrens2011special} edition, now part of \JASON's examples. The two playing teams reach scores that are clearly reduced if even a small amount of sensor misreadings is added ($2.5\%$ --- $10\%$ in the plotted experiment) to the perceptions.

It turns out that \acp{BN} are natural representations of the complex interdependency of random variables and, therefore, great candidates to represent probabilistic beliefs. But the task of replacing symbolic beliefs by \acp{BN} is far from trivial in part because changing the data structure of beliefs entails a chain of reconsiderations about every aspect of the \ac{BDI} architecture. For example, plans have contexts that must be unifiable with the agent's beliefs base; changing the beliefs base from a set of closed formulas to a joint distribution of random variables will break (unchanged) unification with those contexts. 

%The \ac{ASL} as implemented in \JASON\ specifies that the deliberation cycle, depicted in figure \ref{fig:jason.deliberation}, certain selection steps are handled by certain functions. It also defines the signatures of these functions but omits their inner workings. Such omissions play a central role in this work. Despite some work concerning intention selection \cite{bordini2002agentspeak} the default selection function implementation in \JASON\ is a simple process based in round-robin scheduling: intentions form a stack and at each time-step the head action of the top intention is selected; that intention is then sent to the bottom of the stack. This somewhat simplistic approach to selection is good enough for many tasks, including winning planning competitions \cite{bordini2007using,hubner2008developing}.
%
\begin{figure}[t]\begin{center}
		
		\begin{tikzpicture}
			\begin{axis}[smooth,
				width = 0.75\textwidth,
				height = 0.46\textwidth,
				title = {Effect of Noise in Performance},
				xlabel = {Sensor noise}, ylabel = {Gathered golds},
				xmin = -0.005, xmax = 0.105,
				xtick = {0.0, 0.025, 0.05, 0.075, 0.1},
				xticklabel style = {
					/pgf/number format/precision = 3,
					/pgf/number format/fixed,
					/pgf/number format/fixed zerofill,
				},
				mark options = {scale = 2,},
				]
				%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				%
				%	MEAN PLOTS
				%
				%	dummy
				%
				\addplot[solid,black,mark=diamond]
					table[x=NOISE,y=DUMMY] {experiment2.dat};
				\addlegendentry{dummy};
				%
				%	noc
				%
				\addplot[solid,black,mark=square]
					table[x=NOISE,y=NOC] {experiment2.dat};
				\addlegendentry{smart};
				%
				%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				%
				%	MEAN +/- STDVAR PLOTS
				%
				%
				%	dummy
				%
				\addplot[black, dotted, name path = DMIN]
					table[x=NOISE,y=DUMMYMIN] {experiment2.dat};	
				\addplot[black, dotted, name path = DMAX]
					table[x=NOISE,y=DUMMYMAX] {experiment2.dat};
				%
				%	noc
				%
				\addplot[black, dotted, name path = NMIN]
					table[x=NOISE,y=NOCMIN] {experiment2.dat};
				\addplot[black, dotted, name path = NMAX]
					table[x=NOISE,y=NOCMAX] {experiment2.dat};
				%
				%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				%
				%	FILL PLOTS
				%
				%
				%	dummy
				%
				% \addplot[black!10, opacity=0.25]
				% 	fill between [of= DMIN and DMAX];
				%
				%	noc
				%
				% \addplot[black!10, opacity=0.2 	5]
				% 	fill between [of= NMIN and NMAX];
			\end{axis}
		\end{tikzpicture}
%
\caption{
	%
	%CORRECTED: 
	%\review{difficult to read when printed in B/W}
	%
	If sensors report misreadings of the environment state the symbolic inference process ineherent to \ac{BDI} uses false perceptions as (true) facts of the environment and the deliberation process works on wrong assumptions. This plot relates sensor noise (the rate of sensor misreadings, in the horizontal axis) with agent performance measured by the the number of gathered golds (in the vertical axis). Two teams are ploted, the basic reference ``dummy'' that barely uses \ac{BDI} features and the ``smart'' team, fully \ac{BDI}, (designed by \cite{hubner2008developing}) that won the 2006 ``Multi-agent Programming Contest'' \cite{dastani2007second} featuring the \GM\ scenario. 
Each data point summarizes the number of gathered golds by team in a given noise parameter and consists of the mean and standard variation of ten samples. The mean is traced by a thin black line and standard variation by a band centered in the mean value, bounded by dotted lines. Values between datapoints are interpolations.
}
%
\label{fig:experiment1.results}
%
\end{center}\end{figure}
%

Our proposal is, at large, to wrap a layer of probabilistic techniques around certain symbolic processes  without altering those processes or associated semantics. Here we illustrate this approach by focusing on the perception. In a stochastic environment, with a certain probability, values reported by sensors differ from the actual value. If sensor reported values are directly used by a symbolic deliberation process then, as might be expected, performance suffers a penalty that results from the illusions about the truth of the environment.
%
%
%
But sensor misreadings can be partially corrected under certain conditions using probabilistic methods. Our task is to find out if such corrections can be made without sacrificing the assumption of agent autonomy. In particular if the added complexity has little impact in the deliberation cycle.
%

The remainder of this paper is organized as follows: in Section~\ref{section:state_of_the_art} are provided the main concepts of these areas, followed, in Section \ref{sec:probabilistic.perception.correction}, by a general description of the \acf{PCF} in the \ac{BDI} agent architecture and a specific instantiation for the \GM\ scenario extended with sensor misreadings. Section \ref{sec:results} presents a particular experiment on that scenario and respective results. In the last section the authors draw some conclusions on that experiment and outline future research.
