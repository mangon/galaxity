# Revisão do ICAART

* Paper Title: Probabilistic Selection in AgentSpeak(L)

##Review #: 1	

Criterium Description | Value
-|-
Abstract and Introduction are adequate? | Too short
Conclusions/Future Work are convincing? | Yes
Figures are adequate ?	in number and quality | No
Improve critical discussion ?	validation | No
Improve English? | No
Needs comparative evaluation? | No
Needs more experimental results? | No
Originality	Newness of the ideas expressed | 4
Overall Rating	Weighted value of above items | 4
Paper formatting needs adjustment? | Yes
Presentation	Structure/Length/English | 2
References are up-to-date and appropriate? | Yes
Relevance	Paper fits one or more of the topic areas? | 6
Significance	Is the problem worth the given attention? | 4
Technical Quality	Theoretical soundness/methodology | 4

Scale: 1:Lowest Value;6:Highest Value

**Observations for Author**

This paper describes the implementation of an autonomous agent that implies the use of both symbolic data structure together with numeric uncertainty. This uncertainty is introduced in perception or in action effects.

Details :

introduction seems very short maybe it should be merged with the state of the art. 

More precisely in introduction :

* "degree of uncertainty" : on what?
* "such tasks" : what tasks precisely?

* page 1 col 2 line -7: that the deliberation => that in the deliberation

* page 2: Figure 2 is not understandable and not explained (what are dS, dD,... D S...)

* col 2 line 1: what does mean 5% noise plotted on actions (you mean possibility of failure?)

* before part 3 you should explain/develop "unification with those contexts".

* you should also describe the organisation of your paper (perhaps just before section 3) and explain your choice to extend logic with proba instead of extending PGM with logics.
* page 3, col 2, line 3 : too long sentence
* Conclusion : how the coefficient of the nodes can be learned by experience? could you develop? More general question how are the probability distributions obtained ?


##Review #: 2	

Criterium | Description	Value
- | -
Abstract and Introduction are adequate? | Yes
Conclusions/Future Work are convincing? | Yes
Figures are adequate ?	in number and quality | Yes
Improve critical discussion ?	validation | Yes
Improve English? | No
Needs comparative evaluation? | Yes
Needs more experimental results? | Yes
Originality	Newness of the ideas expressed | 4
Overall Rating	Weighted value of above items | 4
Paper formatting needs adjustment? | No
Presentation	Structure/Length/English | 4
References are up-to-date and appropriate? | Yes
Relevance	Paper fits one or more of the topic areas? | 5
Significance	Is the problem worth the given attention? | 5
Technical Quality	Theoretical soundness/methodology | 3

Scale: 1:Lowest Value;6:Highest Value

**Observations for Author**

The paper aims to contribute to a very interesting and exciting new research area that looks for ways to integrate symbolic (ie. logic & BDI) and sub-symbolic (mostly probabilistic and bayesian) programming techniques and languages, into a new agent programming paradigm. However, it falls short to attain its goals. Indeed, the abstract's ending statement “In this paper, a seminal work, strong theoretical and empirical support is replaced by a simple, yet to be fully resolved, exercise. ” does not help very much. Of course, it is a short paper, but some care must be given to the writing and to the effective contribution of the work. It makes some interesting contributions in terms of applying influence diagrams to specify agent's plans. This contribution is restricted to Figure 3, and this is a point that should be better addressed by the paper. The final discussion and the discussion about related work are also very superficial, lacking a critical and comparative analysis.


##Review #: 3

Criterium | Description	Value
-|-
Abstract and Introduction are adequate? | Too short
Conclusions/Future Work are convincing? | No
Figures are adequate ?	in number and quality | No
Improve critical discussion ?	validation | Yes
Improve English? | No
Needs comparative evaluation? | Yes
Needs more experimental results? | Yes
Originality	Newness of the ideas expressed | 3
Overall Rating	Weighted value of above items | 2
Paper formatting needs adjustment? | Yes
Presentation	Structure/Length/English | 3
References are up-to-date and appropriate? | Yes
Relevance	Paper fits one or more of the topic areas? | 4
Significance	Is the problem worth the given attention? | 3
Technical Quality	Theoretical soundness/methodology | 2

Scale: 1:Lowest Value;6:Highest Value

**Observations for Author**

It is not clear the utilization of the figure 2 in your paper the description of the problem is not clear, and where is your improvement but, there are some techniques to solve dynamical optimization problem which can be used in this context It is a good approach, but it is very know test???



