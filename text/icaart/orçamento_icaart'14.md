# Orçamento ICAART'14

* Conferência: [ICAART'14](http://www.icaart.org/Home.aspx)
* Datas: 6 &mdash; 8 Março


Tipo | €
-|-
Alojamento (3 noites)| 200€
Comboio (ida e volta) Paris &mdash; Angers | 150€
Conferência | 535€ (&leq; 14 Jan); 635€ (> 14Jan)
Avião (ida e volta) Lisboa &mdash; Paris| 160€
**TOTAL** | 1045€ (&leq; 14 Jan); 1145€ (> 14Jan)



