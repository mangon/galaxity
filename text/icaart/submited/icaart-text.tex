\documentclass{llncs}

%\usepackage{geometry} 
%\geometry{a4paper} 
\usepackage{graphicx}
\usepackage{amssymb}

%
%
%	TIKZ and PGF
%
\usepackage{tikz}
%
%	For influence diagrams
\usetikzlibrary{shapes.misc}
%
% common style
\tikzset{idnode/.style={inner ysep = 4pt,inner xsep = 8pt, draw}}
%
% random var
\tikzset{varnode/.style={idnode,rounded rectangle}}
%
% random var
\tikzset{detnode/.style={idnode,rounded rectangle,double}}
%
% utility
\tikzset{utilnode/.style={idnode, chamfered rectangle, chamfered rectangle xsep=32pt, chamfered rectangle ysep=0pt}}
%
% action
\tikzset{actionnode/.style={idnode,rectangle}}
%
% arrows
\tikzset{cond/.style={>=stealth,draw}}
%

%
%	PGFPLOTS
\usepackage{pgfplots}
\pgfplotsset{compat=1.8}
\usepgfplotslibrary{statistics}
%

%
%
%	ACRONYMS
%
\usepackage{acronym}
%
\acrodef{PGM}{Probabilistic Graphical Model}
\acrodef{ML}{Machine Learning}
\acrodef{BDI}{Beliefs, Desires and Intentions}
\acrodef{SRL}{Statistical Relational Learning}
\acrodef{PLP}{Probabilistic Logic Programming}
\acrodef{ASL}[\textsc{ASL}]{\textsc{AgentSpeak(L)}}
\acrodef{BN}{Bayesian Network}
\acrodef{AI}{Artificial Intelligence}
\acrodef{PCA}{Principal Component Analysis}
\acrodef{DNN}{Deep Neural Networks}
\acrodef{CPD}{Conditional Probability Distribution}
\acrodef{MEU}{Maximum Expected Utility}
%

%
%	COMMANDS
%
\newcommand{\JASON}{\textsc{Jason}}
\newcommand{\GM}{\textsc{GoldMiners}}
\newcommand{\WEKA}{\textsc{weka}}
\newcommand{\SAMIAM}{\textsc{samiam}}
\newcommand{\PROLOG}{\textsc{ProLog}}
\newcommand{\GALAXITY}{\textsc{Galaxity}}

\newcommand{\SE}{\ensuremath{\textrm{selectEvent}}}
\newcommand{\SO}{\ensuremath{\textrm{selectOption}}}
\newcommand{\SI}{\ensuremath{\textrm{selectIntention}}}
%\newcommand{\AUTHNOTE}[1]{~\newline~\newline\fbox{\begin{minipage}{0.95\textwidth}
%\textbf{Author note:~}#1
%\end{minipage}}\newline~\newline}
\newcommand{\AUTHNOTE}[1]{\marginpar{\tiny $\blacktriangleright$~#1}}
\newcommand{\GOLDS}{\ensuremath{\mathbf{G}}}
\newcommand{\mathtxt}[1]{\ensuremath{\textrm{#1}}}
\newcommand{\PR}[1]{\ensuremath{\textrm{Pr}\left\lbrace #1 \right\rbrace}}
\newcommand{\AT}[1]{\left(#1 \right)}
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	
%	CONTENTS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
%
\title{Probabilistic Selection in \textsc{AgentSpeak(L)}}\subtitle{the layout of a research project}
%

%
%
\author{
Francisco~Coelho\inst{1}$^,$\inst{2}
%\and
%Vitor~Nogueira\inst{1}$^,$\inst{3}
%\and
%Salvador~Abreu\inst{1}$^,$\inst{4}
}
%

%
%
\institute{
Dept. Inform\'{a}tica, Univ. \'{E}vora, Rua Rom\~{a}o Ramalho 58, 7000-671 \'{E}vora
\and
Laboratory of Agent Modelling (LabMAg), \email{fc@di.uevora.pt}
%\and
%VBN research Institute, \email{vbn@di.uevora.pt}
%\and
%SPA research Institute, \email{spa@di.uevora.pt}
}
%

%
%
\date{\today\ (draft version)}
%

\begin{document}
%
%
\maketitle
%
%
\begin{abstract}
This work proposes a conflict free two layer approach to agent programming that uses already established methods and tools from both symbolic and probabilistic artificial intelligence. 

Agent programming is mostly a symbolic discipline and, as such, draws little benefits from probabilistic areas as machine learning and graphical models. However, the greatest objective of agent research is the achievement of autonomy in dynamical and complex environments --- a goal that implies embracing uncertainty and therefore the entailed representations, algorithms and techniques.

In this paper, a seminal work, strong theoretical and empirical support is replaced by a simple, yet to be fully resolved, exercise.
\end{abstract}
%
%
% Force the generation of all references in the bibliography
\nocite{*}
%
%

\section{Introduction}

Agent autonomy is a key objective in \ac{AI}. Complex and dynamic environments, like the physical world where robots must delve, impose a degree of uncertainty that challenges the vocation of symbolic processing. But while a probabilistic approach --- currently expressed in \ac{ML} and \acp{PGM} --- is required for certain aspects of such tasks, a great deal of agent programming is better handled by declarative programming (\emph{e.g.} \PROLOG) and more specifically, \ac{BDI} architectures for autonomous agents, part of symbolic \ac{AI}.%\AUTHNOTE{\textbf{language}~ are \emph{symbolic}, \emph{logic} and \emph{declarative} being properly used?}

\subsection{State of the art}

Although the symbolic and probabilistic areas of AI correspond, in fact, to different and often antagonist cultures and perspectives, they are not necessarily incompatible.
%
Bridges between them are being built based on distribution semantics \cite{sato1995statistical} or markov random fields \cite{domingos2006unifying}. From that common ground there are two possible paths towards the interplay of symbolic and probabilistic \ac{AI}: the extension of \acp{PGM} \cite{koller2009probabilistic} with lo\-gi\-cal and relational  representations (done by  \ac{SRL}) \cite{sato2011general,sato2007inside} and the extension of logic programming languages with probability (in \ac{PLP}) \cite{Fierens:2013fk,fierens2012inference,gutmann2011learning}.

%
From the point of view of programming autonomous agents the sym\-bo\-lic/pro\-ba\-bi\-lis\-tic division essentially still persists: symbolic architectures, such as \ac{BDI}, describe the behavior of the agents on the basis of metaphors (\emph{e.g.} goals, beliefs) drawn from human behavior while the principle of \ac{MEU} is included, as influence diagrams, in probabilistic  \ac{AI} but there is only seminal work blurring that division.
%\AUTHNOTE{What work?  \cite{rens2010belief}?}

\begin{figure*}[t]
\begin{center}
\begin{tikzpicture}
\node at (0,3) (ENV1) {Percepts};
\node[draw] at (0,2) (BRF) {BRF};
\node[draw, rounded corners] at (1.5,3) (BB) {Beliefs};
\node[draw, rounded corners] at (1.5,1) (EV) {Events};
\node[draw,fill=gray!50!white] at (3.75,1) (SE) {Select event};
\node[draw, rounded corners] at (3.75,4) (PL) {Plans library};
\node[draw] at (3.75,3) (GO) {Generate options};
\node[draw,fill=gray!50!white] at (6.75,3) (SO) {Select option};
\node[draw, rounded corners] at (6.75,2) (IQ) {Intentions};
\node[draw,fill=gray!50!white] at (6.75,1) (SI) {Select intention};
\node at (9,1) (ENV2) {Action};
\path[->]
	(ENV1) edge (BRF)
	(BRF) edge (BB)
	(BB) edge (BRF)
	(BRF) edge (EV)
	(EV) edge (SE)
	(PL) edge (GO)
	(SE) edge (GO)
	(BB) edge (GO)
	(GO) edge (SO)
	(SO) edge (IQ)
	(IQ) edge (SI)
	(SI) edge (ENV2)
;
\end{tikzpicture}
\end{center}
%
\caption{The \JASON\ deliberation process very resumed, with selection functions highlighted. The \ac{ASL} only specifies the signature of the functions omitting any conditions, besides the type, on the output.}
%
\label{fig:jason.deliberation}
\end{figure*}{}
%
Concerning agents programming \JASON\ \cite{bordini2007programming} is a popular \acf{ASL} \cite{rao1996agentspeak} interpreter and framework, triggering a considerable amount of research (\emph{e.g.} \cite{bordini2010semantics,bordini2006bdi}). The \ac{BDI} architecture in general, including \ac{ASL} and \JASON\ in particular, outline a set of symbolic data structures and processes with more or less detailed semantics. The \ac{ASL} as implemented in \JASON\ specifies that the deliberation cycle, depicted in figure \ref{fig:jason.deliberation}, certain selection steps are handled by certain functions. It also defines the signatures of these functions but omits their inner workings. Such omissions play a central role in this work. Despite some work concerning intention selection \cite{bordini2002agentspeak} the default selection function implementation in \JASON\ is a simple process based in round-robin scheduling: intentions form a stack and at each time-step the head action of the top intention is selected; that intention is then sent to the bottom of the stack. This somewhat simplistic approach to selection is good enough for many tasks, including winning planning competitions \cite{bordini2007using,hubner2008developing}.

\begin{figure*}[t]\begin{center}
\begin{tikzpicture}
\begin{axis}[width=0.75\textwidth,height=6cm,
	boxplot/draw direction=x,
	ytick={1,2,3,4},
	yticklabels={\textrm{dummy-S}, \textrm{dummy-D}, \textrm{miners-S}, \textrm{miners-D}},
	ylabel={Team-Scenario},
	xlabel={Score distribution},
	title={Score of \ac{ASL} agents in deterministic (\textrm{-D}) \emph{vs.} $5\%$ noisy scenarios (\textrm{-S})},
	]	
	\addplot[boxplot][fill=gray!25!white] table [y index=2] {experiment1.dat};
	\addplot[boxplot] table [y index=0] {experiment1.dat};
	\addplot[boxplot][fill=gray!25!white] table [y index=3] {experiment1.dat};
	\addplot[boxplot] table [y index=1] {experiment1.dat};
\end{axis}
\end{tikzpicture}
%
\caption{Score distribution of a simple experiment based in the \GM\ scenario. The two top boxes, \textrm{miners-D} and \textrm{miners-S}, refer to the \textrm{miners} team that fully applies \ac{ASL} while the bottom boxes, \textrm{dummy-D} and \textrm{dummy-S}, illustrate a team with simpler behavior. Shaded boxes (the \textrm{miners-S} and \textrm{dummy-S} boxes) summarize a noisy scenario where the rate of environment perception and action error is $5\%$ while clear boxes describe the original noise-free scenario.
These plots reflect data gathered from ten runs in each scenario.
}
%
\label{fig:experiment1.results}
%
\end{center}\end{figure*}
%
However we can see \JASON\ agents in trouble when their environment  becomes stochastic. This assertion is hinted by a simple experiment plotted in figure \ref{fig:experiment1.results}: the \GM\ is a virtual scenario used in the 2006 Multi-Agent Programming Contest \cite{behrens2011special} edition, now part of \JASON's examples. If this scenario is run unchanged the two playing teams reach scores that are clearly reduced if even a small amount of noise is added ($5\%$ in the plotted experiment) to the perceptions and actions.

It turns out that a \ac{BN} is a natural representation of the complex  interdependency of random variables and, therefore, a great candidate to represent   probabilistic beliefs. But the task of replacing symbolic beliefs by \acp{BN} is far from trivial in part because changing the data structure of beliefs entails a chain of reconsiderations about every aspect of the architecture. For example, plans have contexts that must be unifiable with the agent's beliefs base; changing the beliefs base from a set of closed formulas to a joint distribution of random variables will break (unchanged) unification with those contexts. 

\section{Problem Motivation and Paper Overview}

Currently the problem of extending \ac{ASL} data structures with probabilistic features is being addressed by different authors \cite{luzalternatives,fagundes2009deliberation,fagundes2007integrating,kieling2011insertion,silva2011agentspeak} but isn't yet fully solved.
%
An alternative and less intrusive application of probabilistic \ac{AI} to \ac{ASL} targets the processes instead of the data. Bounding probabilistic techniques to the computation of \ac{ASL} selection functions (events, options and intentions\footnote{Recent versions of \JASON\ include an inbox and define a select function of messages.}, as in figure \ref{fig:jason.deliberation}) promises a number of benefits:
\begin{itemize}
\item selection functions usually have natural formulations in terms of optimization problems which, in many cases, are well handled by probabilistic algorithms;
\item since their computations are unspecified (in \ac{ASL} or \JASON) probabilistic techniques can be used without compromising previous work;
\item symbolic and probabilistic \ac{AI} roles are clearly separated but both simultaneously contribute to the agent behavior: 
	\begin{itemize}
	\item symbolic programming uses unchanged \ac{ASL} to define high level agent behavior, with plans, goals, beliefs, resolution,\emph{etc};
	\item probabilistic algorithms use unchanged tools (\emph{e.g.} \WEKA\   \cite{bouckaert2010weka,witten2005data,dimov2007weka,hall2009weka} or \SAMIAM \footnote{Available at \url{http://reasoning.cs.ucla.edu/samiam/}})  to process low level noisy signals --- with \acp{BN}, influence diagrams, monte-carlo markov chains, expectation-maximization, \emph{etc};
	\end{itemize}
%\item theoretical results of probabilistic \ac{AI} (\emph{e.g.} concernig error estimation) can be used to better 
\end{itemize}
%

%
In summary, defining the selection functions of \ac{ASL} as optimization tasks to be solved by probabilistic techniques seems to pose a promising set of open problems, with the potential of great contribute for an evolution (and not a revolution) of agent programming.
%


%%
%\item What \acp{PGM} techniques apply?
%%
%\item In what tools?
%	\begin{itemize}
%	\item \WEKA~ for machine learning and data analysis
%	\item \SAMIAM~ for graphical models
%	\item others, preferably \texttt{java} based, for better integration with \texttt{Jason};
%	\end{itemize}
%%
%\item How to measure and compare with symbolic only agents?
%	\begin{quote}
%	With the competition \textbf{score}, of course! (yes, again)
%	\end{quote}
%\end{itemize}

\subsection{Problem Statement}
%
%	Start this (sub)section with a nice figure.
%
\begin{figure*}[t]
\begin{center}
\begin{tabular}{cc}
(a) Action Noise Model & (b) Perception Noise Model \\
\begin{tikzpicture}
\node[varnode] at (0,1) (S) {\mathtxt{ActionSel}};
\node[varnode,fill=gray!50!white] at (0,0) (N) {\mathtxt{ActionNoise}};
\node[varnode] at (3,1) (E) {\mathtxt{ActionExec}};

\path[->, every edge/.style={cond}]
	(S) edge (E)
	(N) edge (E)
;
\end{tikzpicture}
&
\begin{tikzpicture}
\node[varnode] at (0,1) (GT) {\mathtxt{GoldTrue}};
\node[varnode,fill=gray!50!white] at (0,0) (GN) {\mathtxt{GoldNoise}};
\node[varnode] at (3,1) (GP) {\mathtxt{GoldPer}};

\path[->, every edge/.style={cond}]
	(GT) edge (GP)
	(GN) edge (GP)
;
\end{tikzpicture}
\end{tabular}
\end{center}
%
\caption{%
Simple noise model added to local \GM\ simulations. Only two cases are shown due to size and relevance considerations. In the left panel (a) the action selected by the agent after his deliberation step, denoted by \mathtxt{ActionSel}, can be (uniformly) changed, to \mathtxt{ActionExec}, with a given probability, \mathtxt{ActionNoise}. Specifically, with probability $\PR{\mathtxt{ActionNoise} \not=\mathtxt{yes}}$, $\mathtxt{ActionExec} = \mathtxt{ActionSel}$ and, with probability $\PR{\textrm{ActionNoise}=\mathtxt{yes}}$, $\mathtxt{ActionExec}$ is uniformly picked from the repository of possible actions. Perception noise uses the same mechanism with different parameters. The right panel (b) depicts the particular case of gold perception. The real existence of gold is represented by \mathtxt{GoldTrue}, noise in the perception of gold by \mathtxt{GoldNoise} and gold perception by \mathtxt{GoldPer}. 
}
%
\label{fig:noise.model}
%
\end{figure*}
%

%
Our goal is to use the original \ac{ASL} \JASON\ programs of the miners team, the stochastic environment of the experiment depicted in figure \ref{fig:experiment1.results} and compute the intention selection with a probabilistic process based in the \ac{MEU} principle, detailed above. Success achieving this goal can be directly measured by the effect on the team's performance: the greater the performance increase, the greater the success.
%

%
This paper outlines the design of such probabilistic process. Implementation and evaluation are postponed to future developments. 
%
Next a more precise statement of this problem is preceded by short overviews of the \GM\ competition, noise model used, miners coordination and influence diagrams.
%

%
\emph{The \GM\ competition.} The original environment is partially observable, stochastic, sequential, dynamic and discrete (see \cite{russell2003artificial} about this classification). The competition description states that ``[t]he environment of the multi-agent system was a grid-like world where agents could move from one cell to a neighbouring cell if there was no agent or obstacle already in that cell. In this environment, gold could appear in the cells. Participating agent teams were expected to explore the environment, avoid obstacles and compete with another agent team for the gold. The agents of each team could coordinate their actions in order to collect as much gold as they could and to deliver it to the depot where the gold can be safely stored. Agents had only a local view on their environment, their perceptions could be incomplete, and their actions could fail [\emph{in}  \cite{dastani2007second}].''
%

%
\emph{The added noise model.} Although noise is present in the original competition scenario in the form of incomplete perception and action failure the \GM\ examples in the \JASON\ distribution (besides providing a proxy to that competition simulator) optionally use a local simulator for development and evaluation purposes. In the local simulator noise only increases the probability of action failure in proportion to current cargo. Also the environment type changes from stochastic (where gold can appear in cells) to strategic (because the only changes in the environment state are produced by agents).\AUTHNOTE{How sure I'm about this stochastic to strategic change? In particular, I must \textbf{investigate the relevant source code}.}
%
Experimental results depicted in figure \ref{fig:experiment1.results} result from this local simulator with added noise in the perceptions and actions. This new noise model is partially depicted by the \acp{PGM} in figure \ref{fig:noise.model}. Besides action and gold pieces also position, step number and neighborhood have identical models, easily configurable by \mathtxt{\emph{Var}Noise} variables (\emph{e.g.} \mathtxt{GoldNoise}). In particular when all $\PR{\mathtxt{\emph{Var}Noise} = \mathtxt{yes}} = 0$ the simulator runs the original local simulation.
%

%
\emph{Miners team coordination.} The \GM\ miners team follows a coordination protocol concerning the collection and transport of newly found gold pieces. There are two kinds of agents, a single leader and a set of miners. 
%
``[The] leader helps the miners to coordinate themselves in [\ldots] the negotiation process that is started when a miner sees a piece of gold and is not able to collect it (because its container is full). This miner broadcasts the gold location to other miners who then send bids to the leader. The leader chooses the best offer and allocate the corresponding agent to collect that piece of gold [\emph{in} \cite{hubner2008developing,bordini2006bdi}].''
%

%
\emph{Influence diagrams.} The default \JASON\ intention selection me\-thod, round-robin based, is not much context aware. While extensions like plans ``priority'' annotations can provide cues to more informed choices, action selection is the subject of an huge area of probabilistic \ac{AI} centered around the \ac{MEU} principle (\emph{e.g.} \cite{koller2009probabilistic,russell2003artificial}). Within the \ac{PGM} setting the \ac{MEU} principle is instantiated by influence diagrams, graphical models extended with special nodes to represent utilities and actions.
%Very briefly, in the \ac{MEU} setup the environment is in one state $s \in \mathcal{S}$ where the agent can perform actions $a \in \mathcal{A}$ that change the environment state $s \to s'$ with a certain transition probability $\PR{s' \middle| s, a}$ and receive a certain reward $R\AT{s',s,a}$. For this setting the \ac{MEU} principle selects the action that maximizes the expected utility that results from the reward function and transition probability:
%\begin{eqnarray*}
%a^{\ast} &=& \arg\max_a\: \mathbb{EU}\left[ a,s \right]
%\end{eqnarray*}
%where
%\begin{eqnarray*}
%\mathbb{EU}\left[ a,s \right] &=& \sum_{s' \in \mathcal{S}} \mathbb{U}\left[s' \right] \PR{s' \middle| s, a}\\
%\mathbb{U}\left[s\right] &=& \sum_{s',a \in \mathcal{S},\mathcal{A}} \AT{\alpha \mathbb{U}\left[s' \right] + R\AT{s',s,a}}\PR{s' \middle| s,a}
%\end{eqnarray*}
%with a discount factor typically $\alpha < 1$.
%

%
\emph{Problem statement.} To describe the miners intention selection function using an influence diagram where deposited golds is an utility node and the range of actions is extracted from the active intentions.
%

%
%
\subsection{Problem Resolution}
%
%	Start this (sub)section with a nice figure.
%
\begin{figure*}[t]
\begin{center}
\begin{tikzpicture}
\node[utilnode] at (0,0) (DG) {Deposited};
\node[varnode] at (0,1) (TTG) {Transported (team)};
\node[varnode] at (2,2) (ATG) {Transported (agent)};
\node[varnode] at (0,3) (SG) {Assigned};
\node[varnode] at (0,4) (FG) {Detected};
%\node[varnode, fill=gray!50!white] at (-2,3) (HG) {Hidden };
\node[actionnode] at (5,3) (A) {Selected Intention};

\path[->, every edge/.style={cond}]
	(SG) edge (ATG)
	(ATG) edge (TTG)
	(SG) edge (TTG)
	(TTG) edge (DG)
	(FG) edge (SG)
%	(FG) edge (HG)
	(A) edge (FG)
	(A) edge (SG)
	(A) edge (ATG)
	(A) edge[bend left] (DG)
;
\end{tikzpicture}
\end{center}
%
\caption{%
A possible influence diagram for the discovery, auctioning, transportation and deposit of gold pieces. Variables (denoted by round rectangles) and Utilities (chamfered rectangles) in this diagram refer to \emph{total quantities} of gold pieces. The probabilistic effect of the selected action is represented by the arrows that leave the action (rectangular) node.
}
%
\label{fig:influence.diagram.1}
%
\end{figure*}
%

%
%
\AUTHNOTE{From this point on the text is too drafty. What do you mean with ``\emph{it is also drafty up to this point.}``?}
%
%
%
The process along the miners coordination protocol can be represented by an influence diagram starting in the discovery of a new gold piece and terminating with the deposit of that gold piece, as the one depicted in figure \ref{fig:influence.diagram.1}, and the resulting utility function can then be used by the \ac{MEU} principle to select, from the available actions (the heads of instantiated plans in the intentions stack) the optimal one.
%

%
\begin{enumerate}
%
\item Take the detect-$\ldots$ process and direct to the influence diagram in figure \ref{fig:influence.diagram.1};
\begin{quote}
be careful to not confuse ``reward'' and ``utility''!
\end{quote}
%
\item Justify the choice of the ``intention selection'':
\begin{quote}
This choice rests on the observation that its role in the \JASON\ deliberation cycle can be described quite naturally by an influence diagram, as the one depicted in figure \ref{fig:influence.diagram.1}. 
\end{quote}
%
\item Outline that the \acp{CPD} are still undefined;
%
\end{enumerate}

\emph{Try to minimize effort} by

\begin{enumerate}
\item minimal scenario adaptation (done, in principle)
\item simplest possible application of \ac{PGM}/\ac{ML}
\item usage of existing libraries
\end{enumerate}


The influence diagram depicted in figure \ref{fig:influence.diagram.1} summarizes the key process that leads to the utility

\section{Results Report}

No (hard, numerical) results are expected in the current phase.

\section{Conclusion}

%
Also important is the communication between symbolic and probabilistic levels. Selection functions signatures already define some channels: arguments are information traveling from the symbolic to the probabilistic level while the returned values work the other way around. But perhaps other forms of mutual influence can be considered. For example, a process where at the probabilistic level influence diagrams (\emph{e.g.} \ac{BN}) are structured from the set of symbolic plans and beliefs and the coefficients in the factors of the nodes are learned from experience. One can also imagine a similar influence going from the probabilistic to the symbolic level, either introducing relevant concepts from unsupervised feature learning \cite{socher2011parsing} using \ac{DNN} \cite{hinton2007learning,salakhutdinov2012efficient} or summarizing \ac{ML} techniques like \ac{PCA} \cite{jolliffe2005principal}.
%

%
This two-layer approach also raises some practical and theoretical concerns. In particular, since probabilistic deliberation algorithms are framed in a game-theory setting based in either a reward, utility or cost function, one as to ask how to define such functions. For example, in the \GM\ competition the team score is a natural reward (although not an utility) function.
\AUTHNOTE{Must say something more concerning those concerns.}
%

%
This is an outline for a 1 -- 2 year research project:%
\AUTHNOTE{\textbf{VBN:} Social communication in \JASON\ is represented by messages that must be selected from the inbox and composed and sent to the outbox (not sure about this last part). If a \ac{BN} represents the reputation and expectations about other agents, bayesian learning updates the social reasoning, \emph{etc}. I'm quite sure that there is an immense area of probabilistic social reasoning to bring here. \textbf{Teams of mobile robots} can climb in the priority queue\ldots}
%


\begin{enumerate}

\item Consider a scenario where the noise is added to the communications;

\item Use a \ac{PGM}/\ac{ML} tool, like \WEKA, to define one of the selection functions in a \GM\ member in the stochastic environment used in experiment of figure \ref{fig:experiment1.results} and out-perform the default team.

\item Adapt the work of the previous step to new scenarios;

\item Generalize and synthesize the gathered experience;

\item Consider a simple application to a single mobile robot --- with Kalman filters, PID controllers, \emph{etc.};

\item Consider a complex application to a team of mobile robots;

\item Consider complex applications to autonomous planes;

\end{enumerate}
%

%
%
\section*{Acknowledgements}
The glorious, the people around us, the flow of experiences. And, of course, all money providers.
%
%
\bibliographystyle{plain}
\bibliography{dapaiper}


\end{document}  