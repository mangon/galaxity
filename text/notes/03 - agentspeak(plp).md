# AgentSpeak(PLP)

O **objectivo** deste texto é determinar os elementos da arquitectura `Jason` que pretendemos transformar numa versão probabilística. São resumidas duas classes de possibilidades: sobre os elementos declarativos e sobre os processos de selecção.

Aparentemente, a melhor opção será sobre os processos de selecção considerando que

* a re-escrita das crenças como redes bayesianas já está a ser trabalhada pelo João Gluz, no Brasil;
* as alterações nos elementos declarativos afectam profundamente todo o processo e, por isso, tornam a análise complexa;
* além de definir as crenças como redes bayesianas, não é evidente como aplicar aos elementos declarativos técnicas importantes como os diagramas de influência, algoritmos como EM, etc
* Por outro lado, os processos de selecção no `Jason` parecem candidatos adequados a uma abordagem probabilística. A computação destes processos
    * não está especificada;
    * é independente do restante processamento;
    * pode ser naturalmente formulada como um problema de optimização;
    * potencialmente é investigação original;

Portanto, a **conclusão** é que o melhor percurso de trabalho passa por formular as funções de selecção como problemas de optimização.

## Adaptação Estocástica

#### Elementos declarativos
Na versão "apenas lógica" cada

* **crença** é uma fórmula fechada;
* **plano** é uma estrutura de fórmulas;
* **evento** é uma fórmula fechada;
* **intenção** é uma pilha de fórmulas fechadas (não estou certo se são mesmo fechadas…); 

A adaptação mais óbvia é descrever as **crenças** com uma rede bayesiana. Assim a actualização de crenças passa a ser uma actualização bayesiana e os valores das variáveis são distribuições obtidas por um inquérito estocástico.

>Supondo que as crenças são descritas por um conjunto de variáveis $$$B_1,\ldots, B_p \subseteq X_1, \ldots, X_n$$$, em que $$$X$$$ é um campo de markov (uma rede bayesiana é um caso particular), após uma observação $$$e_1, \ldots, e_m$$$ das variáveis $$$E_1, \ldots, E_m \subseteq X_1, \ldots, X_n$$$, pretendemos obter a distribuição
$$ \textrm{Pr}\left(B\middle|Y,E=e\right)$$
em que $$$Y=X\setminus \left(E \cup B\right)$$$.

Esta adaptação tem consequências profundas na semântica da `AgentSpeak(L)` pois  afectam todo o processo. Portanto uma alteração nestes elementos torna a análise da comparação de desempenhos bastante complexa.

#### Processos de Selecção 
Outra adaptação seria usar técnicas probabilísticas para definir as várias funções de selecção especificadas na arquitectura `AgentSpeak(L)`. De facto a `AgentSpeak(L)` é omissa em relação aos detalhes dessas funções.

> Por exemplo a função `selectOption` instancia um plano escolhido entre todos os planos disponíveis (`applicable`) na altura. A escolha desse plano pode ser feita através do princípio da maximização da utilidade esperada.

De facto, no `Jason`, as funções de selecção estão todas definidas como métodos da mesma classe, `jason.asSemantics.Agent`, e limitam-se a retirar e devolver o topo das respectivas pilhas de eventos, opções e intenções.

Dos exemplos que fazem parte da distribuição, os métodos de selecção definidos na classe `jason.asSemantics.Agent` foram alterados apenas em dois casos, e de forma trivial:

* `Airport` de forma a dar prioridade a `unattended_luggage`;
* `gold-miners-II` de forma a preferir eventos `cell(_,_,gold)`;


## O Processo BDI/AS (em _ascii art_)

### 1. Actualização das crenças

    ENV  -->  [brf]  -->  (events)
              [   ]  <=>  (beliefs)

### 2. Atenção
                                        
    (events)  -->  [selectEvent]  -->  [u1]  -->  [u2]  -->  (applicable plans)
                   (plan library)  -->  [  ]       [  ]  <--  (beliefs)
                   
### 3. Deliberação

    (applicable plans)  -->  [selectOption]  -->  (intentions)
    
### 4. Agir

    (intentions)  <=> [selectIntention]  --> (action) --> ENV
    (beliefs)     --> [                ]

### Os conceitos

notação | conceito
--------|---------
 `ENV` | ambiente
 `[brf]` | _belief revision function_
 | actualiza as crenças com base nas percepções
 | e gera eventos para o ciclo que se inicial
`(events)` | os eventos discriminam os planos do agente
`(beliefs)` | o que o agente pensa sobre o estado do ambiente
`[select event]` | foca os passos seguintes num único evento
`(plan library)` | arquivo de conhecimento prático (_know how_)
| catalogado por eventos
| um plano descreve os efeitos e pré-condições das acções
`[u1]` | filtra os planos relevantes para o evento escolhido
`[u2]` | filtra os planos que podem ser usados no estado actual do ambiente
`[select option]` | escolhe o plano que vai ser aplicado
`(intentions)`| planos que o agente mantém em processo de execução
`[select intention]` | escolhe a próxima acção a ser executada 


