# Galaxity: _bayesian agentspeak_

| **Francisco Coelho** | **Vitor Nogueira** |
|----------------------|--------------------|
| `fc@di.uevora.pt` | `vbn@di.uevora.pt` |

**Abstract.**  

Pretendemos encontrar um nicho que misture

* _Probabilistic Graphical Models_ para tratar da representação, inferência e aprendizagem em incerteza;
* _Belief-Desire-Intention Architectures_ para orientar o _design_ do comportamento de agentes autónomos;
* _Declarative Programming_ para os detalhes de implementação; 
* _Decentralized Computing_ para o cenário comum de aplicação;



## 1. Introduction

O tema deste trabalho implica caminhar entre, um lado, a inteligência artificial _estatística_ que actualmente exprime-se na _machine learning_ (ML) e nos _probabilistic graphical models_ (PGM) e, no outro lado, a inteligência artificial _simbólica_, que dá corpo, em geral, à _programação declarativa_ (e.g. o `prolog`) e em particular às arquitecturas _beliefs, desires and intentions_ (`BDI`) para agentes autónomos.

Embora as áreas _estatística_ e _simbólica_ da IA correspondam, de facto, a  culturas e perspectivas diferentes e muitas vezes antagonistas, não são necessariamente incompatíveis. Dois exemplos da aproximação entre estas áreas estão na extensão dos modelos gráficos com representações lógicas e relacionais (_statistical relational learning_, `SRL`),  e na extensão das linguagens de programação lógica às probabilidade (_probabilistic logic programming_, `PLP`).

Do ponto de vista de programação de agentes autónomos temos por um lado as arquitecturas simbólicas, como a `BDI`, que descrevem o comportamento dos agentes com base em metáforas inspiradas no comportamento humano e, por outro lado, o princípio da maximização da utilidade esperada, incluído, sob a forma de _influence diagrams_, nos `PGM`.

> Uma questão que me persegue é se, extendendo o `prolog` à inferência estatística, ainda restará lugar à unificação? Se sim, qual? e, se não, se isso é "mau"?

### 1.1 State of the Art

* O grupo "belga", que trabalha nos [ProbLog[1|2]](http://dtai.cs.kuleuven.be/problog/) tem uma linha de investigação interessante: extender o `prolog`, e a programação lógica em geral, aos `PGM`. A produção deste grupo também é bastante volumosa, com vários artigos _não triviais_ e duas versões do `ProbLog`. A tarefa deles é **centrada na linguagem (e respectiva semântica)** e os agentes nunca são considerados;

    * No site do `ProbLog` estão várias referências para outros sistemas de `SRL` e `PLP`;

    * Há outra abordagem que vai em sentido contrário, no sentido de dotar os `PGM` com representações lógicas;
    
* O grupo "brasileiro", da [Rosa Vicari](http://www.inf.ufrgs.br/~rosa/index.html) tem bastante trabalho de exploração preliminar, em particular usando o [Jason](http://jason.sourceforge.net/wp/) como base para agentes `BDI` programados em `AgentSpeak`. No entanto o resultado visível é um conjunto de vários artigos de "conversa fofa" em `BDI+BN` ;
    * No entanto os `PGM` não se limitam a `BN` e, de facto, nem sempre as `BN` são a representação mais adequada. Alternativas incluem, por exemplo, os `MRF`, os `CRF` e outros;
    * **nunca** são discutidas as tarefas típicas dos `PGM`(`marg`, `map`,  _etc_) nem a aplicação dos algoritmos ( `variable elimination`, `message passing` ou `MCMC`, `ME`, _etc_);
    * **nem** outros "goodies" dos PGM como, _e.g._ os _influence diagram_ para representar deliberação por utilidades;
* No [artigo da wired sobre o WWZ :D](http://www.wired.com/underwire/2013/07/wwz-digital-zombies/) é referido o uso de `agentes inteligentes`;
* E há [outro artigo](http://video.wired.com/watch/the-window-robot-economy) sobre robots numa tarefa distribuída;
    
* "_[SamIam](http://reasoning.cs.ucla.edu/samiam/) is a comprehensive tool for modeling and reasoning with Bayesian networks, developed in Java by the Automated Reasoning Group of Professor Adnan Darwiche at UCLA._"
* "_[Jason](http://jason.sourceforge.net/wp/) is an interpreter for an extended version of AgentSpeak. It implements the operational semantics of that language, and provides a platform for the development of multi-agent systems, with many user-customisable features_."

### 1.2 Problem Motivation & Paper Overview

**Motivação**

1. O uso de `PGM` e `ML` na **programação lógica de agentes autónomos ainda é um assunto de pouco explorado** e com bastantes vias abertas à exploração, como ficou enumerado acima. 

## 2. Problem Statement & Resolution

### 2.1 Problem Statement

### 2.2 Resolution Exposition

## 3. Results Report

## 4. Conclusion

## References

1. Daan Fierens, Guy Van den Broeck, Joris Renkens, Dimitar Shterionov, Bernd Gutmann, Ingo Thon, Gerda Janssens, Luc De Raedt, _Inference and learning in probabilistic logic programs using weighted Boolean formulas_, [arXiv:1304.6810](http://arxiv.org/abs/1304.6810), (2013)
