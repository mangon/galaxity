#Tarefa: Simulador de Ambiente


**Sumário**  
Pretende-se escolher, definir e implementar um conjunto de ambientes _partially observable and stochastic_ para o `Jason`, de forma a ilustrar as diferenças de desempenho entre agentes "puramente lógicos" (`AgentSpeak(L)`) e agentes "lógicos e probabilísticos" (`AgentSpeak(PLP)`).  

Esta escolha deve ficar limitada aos exemplos que fazem parte do `Jason`, não só porque assim (em vez de implementar tudo) basta alterar o que já existe como também as comparações de desempenho são feitas em relação a trabalho prévio, independente.  

Importa resalvar que **um ambiente estocástico não garante uma deliberação estocástica**. O interesse aqui resume-se a criar cenários com vista a  justificar uma BDI estocástica através do desempenho superior em relação às BDI "apenas lógicas" com variantes **mais realistas** de cenários virtuais usados para estudar as BDI "apenas lógicas". Para conferir verosimilhança as variantes têm de manter a compatibilidade para os agentes "apenas lógicos".


## Escolha de um exemplo no Jason

Exemplos que estão na distribuição do `Jason` e que, não sendo tutoriais, pretendem resolver algum problema (ainda que meramente ilustrativo):

1. Airport
2. Cleaning-Robots
3. Domestic-Robot
4. Food-Simulation
5. Gold-Miners e Gold-Miners-II
6. Wumpus

Destes, os seguintes tratam de problemas bem conhecidos e estudados:

1. Cleaning-Robots
2. Gold-miners e Gold-Miners-II
3. Wumpus


Exemplo | Características | Pontos 
--------|-----------------|:-----:
**Cleaning-robots** | | **6**
| implementa "Mars Explorers"| +1 |  
| descrito no livro "Jason" (?)| +5(0)
**Gold-Miners-* **| | **30**
| detalhado no livro "Jason" | +20 |
| usado na competição CLIMA | +10 |
**Wumpus** | | **10**
| bem conhecido do AIMA | 10


A tabela de comparação acima torna evidente a escolha do exemplo a adaptar:
<div style="font-size:18pt;color:crimson;text-align:center;font-weight:bold">
<hr>Gold-Miners<br><p><hr></div>

## Definir a variante estocástica do ambiente

A forma mais natural de fazer uma variante estocástica de um ambiente determinista consiste em adicionar ruído às percepções e às acções. Em particular:

1. Com uma certa probabilidade cada acção "falha";
2. Com uma certa probabilidade cada percepção está errada;


## Lista de Exemplos

#### airport

* multi-agente, heterogéneo
* usa/ilustra `negociação` de alocação de tarefas, `cooperação`
* referência: Rafael H. Bordini, Mehdi Dastani, Jurgen Dix, and Amal El Fallah
   Seghrouchni, editors. Multi-Agent Programming: Languages, Platforms
   and Applications. Chapter 1. Springer-Verlag, 2005


#### auction

* exemplo básico de leilões no `Jason`

#### blocks-world

* exemplo clássico de planeamento
 
#### cleaning-robots

* Exemplo descrito no capítulo 2 do manual do `Jason` (parece o _Mars Explorer_!!!)

#### contract-net-protocol

* ilustração de implementação de `contract  net protocol` em `Jason`

#### domestic-robot

* robot que serve cerveja ao dono;
* exemplo simples de um ambiente em grelha com três agentes em interacção;  

#### food-simulation

* baseado em [Normative reputation and the costs of compliance](http://jasss.soc.surrey.ac.uk/1/3/3.html)

#### game-of-life

* baseado no jogo da vida conforme [NetLogo Models Library: Sample Models/Computer Science/Cellular Automata](http://ccl.northwestern.edu/netlogo/models/Life)

#### gold-miners

* "This paper describes a team of agents that took part in the second CLIMA Contest [and won]."
* equipas de agentes em competição pela extracção e transport dos recursos numa certa área
* a localização dos recursos é desconhecida

#### gold-miners-II

* aparentemente, uma versão mais sofisticada de `gold-miners`, que ilustra com mais sobre a plataforma `Jason`

#### iterated-prisoners-dilemma

* ilustra o bem conhecido problema, numa aplicação do `Jason` em que não há ambiente (!) mas apenas trocas de mensagens

#### mining-robots

* um pequeno exemplo de exploração e recolha de recursos

#### room

* exemplo de convivência entre agentes com objectivos incompatíveis

#### snifer

* exemplo de um agente que observa o estado interno do ambiente

#### wumpus

* ilustra o bem conhecido exemplo no livro AIMA, de Russel & Norvig




