PASL
====

Components
----------

Agent
* Beliefs
* Plans
* Goals

Beliefs
* (set of) probabilistic facts

Plans
* (set of) probabilistic rules (with utility?)

Goals
* (set of) facts (with utility?)

Deliberation Process
--------------------

```
                    +-----------+
                    |           |
                    V           |
[Perceptions] --> (BRF) --> [Beliefs] --+
                                        |
                            [Plans] ----+--> (DEL) --> [Action]
                                        |
                            [Goals] ----+
```

Example: Gold miners
--------------------

Problem statement: Gather gold. If current location has gold, gather it. Otherwise, move to an adjacent location. Repeat until storage is full. Then return home (initial location)

### Deterministic resolution

```
location(X), gold_spoted, move, 
```

Initial Beliefs
```
at_home.
~storage_full.
```

Goals
```
at_home.
storage_full.
```

Plans
```
storage_full [gold_spoted] :-
	gather_gold().

storage_full [~gold_spoted] :-
	!gold_spoted.

gold_spoted :- 
	move.

at_home [storage_full] :-
	go_home.

```

### Probabilistic resolution
Now suppose that perception, actions and the world state are stochastic.

```
0.9::storage_full [gold_spoted] :-
	gather_gold().
/* Semantic (?):
	Prob( storage_full | gold_spoted, by(gather_gold()) ) = 0.9
*/
storage_full [~gold_spoted] :-
	!gold_spoted.
/* Prob(storage_full | ~gold_spoted, by(gold_spoted)) = 1.0
0.2::gold_spoted :- 
	move.

at_home [storage_full] :-
	go_home.
```