Conferences suitable for this work
====================

## Conferences

* International Conference on Autonomous Agents and Multiagent Systems
    * Classf:  A*
    * Web: [AAMAS 2014](http://aamas2014.lip6.fr/)
* Pratical Aspects of Declarative Languages
    * Classf:  B
    * Web: [PADL 2014](http://www.ist.unomaha.edu/padl2014/)
* Computational Logic and Multi-Agent Systems
    * Classf:  B
    * Web: [CLIMA 2013](http://centria.di.fct.unl.pt/events/climaXIV/) **já passou o deadline para submetermos :( **
* Declarative Agent Languages and Technologies
    * Classf:  B
    * Web: DALT?
* IEEE/WIC/ACM International Conference on Intelligent Agent Technology
    * Classf:  B
    * Web: IAT 2013](http://cs.gsu.edu/wic2013/iat)
* IEEE Symposium on Intelligent Agents
    * Classf:  C
    * Web: [IA 2014](http://www.ieee-ssci.org/IA.html)
* International KES Symposium on Agents and Multiagent systems - Technologies and Applications
    * Classf:  C
    * Web: [AMSTA 2013](http://amsta-13.kesinternational.org/)
* Practical Application of Intelligent Agents and Multi-Agent Technology Conference
    * Classf:  C
    * Web: [PAAMS 2014](http://www.paams.net/)
* International Conference on Principles of Practice in Multi-Agent Systems
    * Classf:  C
    * Web: [PRIMA 2013](http://prima2013.otago.ac.nz/)
* International Workshop Programming Multi-Agent Systems
    * Classf:  C
    * Web: [PROMAS](http://www.cs.uu.nl/ProMAS/)
* European Workshop on Multi-Agent Systems
    * Classf:  C
    * Web: [EUMAS 2013](http://www.irit.fr/EUMAS2013/)
* International Conference on Agents and Artificial Intelligence
    * Classf:  C
    * Web: [ICAART](http://www.icaart.org/)

## Journals

**see more on** [agent-based-models.com](http://www.agent-based-models.com/blog/resources/journals/)

* [Adaptive Behavior](http://adb.sagepub.com)
    * freq: ?quartely
    * IF: ~1.113
    * SJR: 0.53

* [Journal of Artificial Societies and Social Simulation](http://jasss.soc.surrey.ac.uk/JASSS.html)
    * freq: quartely
    * IF: ~1.234
    * SJR: 0.39

* [Autonomous Agents and Multi-Agent Systems](http://www.springer.com/computer/ai/journal/10458)
    * freq: bimonthly
    * IF: ~2.103
    * SJR: 1.53

* [ACM Transactions on Autonomous and Adaptive Systems](http://taas.acm.org)
    * freq: ?
    * IF: ~1.364
    * SJR: 1.52

* [Adaptive Behavior](http://adb.sagepub.com)
    * freq: ?quartely
    * IF: ~1.113
    * SJR: 0.53

* [Journal of Artificial Societies and Social Simulation](http://jasss.soc.surrey.ac.uk/JASSS.html)
    * freq: quartely
    * IF: ~1.234
    * SJR: 0.39

* [Web Intelligence and Agent Systems](http://wi-consortium.org/wicweb/html/journal.php)
    * freq: quartely
    * IF: ?
    * SJR: 0.39

* [International Journal of Agent-Oriented Software Engineering](http://www.inderscience.com/jhome.php?jcode=ijaose)
    * freq: quartely
    * IF: ?
    * SJR: 0.21

* [Multiagent and Grid Systems](http://www.iospress.nl/journal/multiagent-and-grid-systems/)
    * freq: quartely
    * IF: ?
    * SJR: 0.11

* [International Journal of Agent Technologies and Systems](http://www.igi-global.com/journal/international-journal-agent-technologies-systems/1109)
    * freq: quartely
    * IF: ?
    * SJR: ?