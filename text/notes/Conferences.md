Conferences suitable for this work
====================

* International Conference on Autonomous Agents and Multiagent Systems: A*: [AAMAS 2014](http://aamas2014.lip6.fr/)
* Pratical Aspects of Declarative Languages: B : [PADL 2014](http://www.ist.unomaha.edu/padl2014/)
* Computational Logic and Multi-Agent Systems: B: [CLIMA 2013](http://centria.di.fct.unl.pt/events/climaXIV/) **já passou o deadline para submetermos :( **
* Declarative Agent Languages and Technologies: B: DALT: ?
* IEEE/WIC/ACM International Conference on Intelligent Agent Technology : B: [IAT 2013](http://cs.gsu.edu/wic2013/iat)
* IEEE Symposium on Intelligent Agents: C: [IA 2014](http://www.ieee-ssci.org/IA.html)
* International KES Symposium on Agents and Multiagent systems - Technologies and Applications: C: [AMSTA 2013](http://amsta-13.kesinternational.org/)
* Practical Application of Intelligent Agents and Multi-Agent Technology Conference: C: [PAAMS 2014](http://www.paams.net/)
* International Conference on Principles of Practice in Multi-Agent Systems: C: [PRIMA 2013](http://prima2013.otago.ac.nz/)
* International Workshop Programming Multi-Agent Systems: C: [PROMAS](http://www.cs.uu.nl/ProMAS/)
* European Workshop on Multi-Agent Systems: C: [EUMAS 2013](http://www.irit.fr/EUMAS2013/)
* International Conference on Agents and Artificial Intelligence: C: [ICAART](http://www.icaart.org/)
