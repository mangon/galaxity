# Correcting Miner's Perceptions

## Introduction
A miner in a grid world seeks for gold pieces. At each time he sees a `3x3` grid around his current position. Content of each cell is one of: `0: empty, 1: obstacle, 3: gold, 4: miner` (in the original problem formulation `miner` folds to `ally` and `enemy`). Furtermore, the actions are limited to `0: up, 1: down, 2: left, 3: right, 4: drop, 5: pick, 6: skip`.

The miner's sensors and actuators are faulty. Can we improve his perception?

```
Scan grid:
[1][2][3]
[4][5][6]
[7][8][9]
```

## Problem statement

### Noise model
Lets start by setting the noise model: 

* `T1`: the probability that a cell scan is wrong;
* `T2`: the probability that the action failed;
* `T3`: the probability that (another) miner moves;

The result of a faulty scan or action can be precisely defined later. For example, the result can be a constant value (e.g. `empty` for scan and `skip` for action), uniformly chosen from a set of possibilities, _etc_.

### States representation

The variables we are concerned are:

* The observed neighborhood: `Y1, ..., Y9`. The miner actual position is in cell 5;
* The real neighborhood state: `X1, ... X9`;
* The miner's previous action: `A`;
* The previous (before the action was executed) belief: `Z1, ..., Z9`;

The story is as follows: A miner believes that his neighborhood is in state `Z1:9` and chooses action `A`. After execution (that action or another one due to malfunction) the neighborhood state becomes `X1:9` but the scan reports `Y1:9`.

We want do estimate `MAP(X1:9 | Z1:9, A, Y1:9)`

### Dimension reduction

The grid disposition and the miner's motions define the parents of each `Xi` (besides the previous action):

`i` | `j: Zj in pa(Xi)`
----|----------
1 | 1, 2, 4
2 | 1, 2, 3, 5
3 | 2, 3, 6
4 | 1, 4, 5, 7
5 | 2, 4, 5, 6, 8
6 | 3, 5, 6, 9
7 | 4, 7, 8
8 | 5, 7, 8, 9
9 | 6, 8, 9

The grid structure of this network has many symmetries that can be utilised to reduce the complexity of the problem. In particular, _modulo_ rotations, there are only three cases for each `Xi`:

* in a corner: `X1, X3, X7, X9`;
* in the middle: `X2, X4, X6, X8`;
* in the center: `X5`;

### Model evaluation

Suppose that a miner is evoling in a given, populated, simulated environment. We can compare the neighborhood real state and the prediction and sum the number of errors per turn.