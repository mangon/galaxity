# UAV and Robotics

## Overview


We start by adopting a general model that defines a stack of layers:

1. World + Hardware (Robot/Drone)
2. LL Control (basic actuators and sensor API)
3. HL Control (motion and sensors API)
4. State estimation (compute `p(s)`)
5. Deliberation (select an action `a` from state estimate `p(s)`)

For example, 

1. World + Hardware = this world with our AR.Drone 2.0
3. LL Control = the stack that Parrot places in this hardware
4. HL Control = YADrone
5. State Estimation = WIP by fc
6. Deliberation = WIP by vbn

We are interested in the **State estimation** and **Deliberation** levels and their interplay.

## State estimation

The task here is to get the feed from the previous level (_HL Control_) and produces a probabilistic current state estimation, _i.e._ compute the distribution `p(s') = p(s' | s, a)` where `s'` is the current state, `s` the preceeding state and `a` the last executed action.

For this task the feed from the level below (HL Software) has the following components:

1. Video (front camera, lower camera)
2. Pose
	- Attitude (pitch, roll, yaw)
	- Velocity (vx, vy, vz)
	- Altitude (z)
3. Internal state
	- Battery level
	- Mode (hovering, landed, emergency, _etc_)
4. _maybe, with the **navigation** module,_ Global Position
	- GPS coordinates (lat, long)

The level above (Deliberation) also produces a feed of commands that must be observed in order to enable state update.

### Task: Drone in a box

> Suppose that the world is a box with only the drone inside it (_and that cows are perfect spheres ;-)_). At any given moment the state is a tuple `(pitch, roll, yaw, a, b, u, v, x, y)` where `pitch, roll, yaw` are given from the attitude sensors and `a, b, u, v, x, y` are the distances the center of the drone is from, resp., the front, back, top, bottom, left and right faces of the box.

> The initial dimensions of the box are unknown and the faces are not static: `a+b`, `u+v` and `x+y` **are not constants**. Actions are `takeoff, land, hover, up, down, left, right, turnleft, turnright, incspeed, decspeed` and the sensors include the video, pose and internal state sets.

> The drone starts in the floor (`v = 0`). **Estimate the current state of the drone**.

For a first resolution of this task let's suppose that the _cube state_ variables (`a,b,u,v,x,y`) are continuous with gaussian distributions (`p(i) ~ N(mean[i], stdvar[i])`) and that the _pose state_ variables are noise-free, with values given by the respective sensors. Assuming that the `stdvar[i]` are constant and known, **Update the `mean[i]` values**.

### Task: _Le Penseur_

> Implement a functional deliberation layer.

### Task: Catch the ball, Fido

> In the "Drone in a box" scenario a red ball is tossed and the drone must follow it.

### Task: State estimation and Deliberation interplay

> Assuming that deliberation is based in "plans", study their compatability with the world state evolution model used in the state estimation layer. In particular answer the following questions:

> - What does it mean "a plan is incompatible with the world model"?
> - What does it mean "a plan is entailed by the world model"?
> - Can plans help in the current state estimation? If so, how?
> - Can the world model help tunning plans?
