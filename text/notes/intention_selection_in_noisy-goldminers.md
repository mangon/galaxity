# Intention Selection in Noisy-GoldMiners

The semantics for the intention-selection influence diagram in the _noisy goldminers_ experiment is defined. In particular is explained what do the nodes in the ID mean, how the distributions and values are computed, _etc_

## Definitions

The expression "in the near future" (ITNF) represents the time interval that starts in the current time-step and expands for a fixed ammount of time steps (e.g. 10 cycles). The amount of time-steps to "look ahead" becomes a parameter, `NF`, of this model. Also AGWB abbeviates "A gold will be ...".

The **nodes** are:

* `Detected` (Uncertain): AGWB detected by the agent ITNF;
* `Assigned` (Uncertain): AGWB assigned to the agent ITNF;
* `Transp.Agent` (Uncertain): AGWB transported by the agent ITNF;
* `Transp.Team` (Uncertain): AGWB transported by some agent of the team ITNF;
* `Deposited` (Value): AGWB deposited in the depot by some agent of the team ITNF;
* `Intention` (Decision): Action that can be executed now by the agent;
 
The values of the `Intention` variable are the actions available to a gold-miner: `skip`, `pick`, `drop`, `up`, `down`, `left`, `right`. All other variables are boolean. These ranges might be enhanced later, in a more sofisticated model. 

The **arcs** are defined by conditional distributions:

* `P( Detected | Intention )`
* `P( Assigned | Detected )`
* `P( Transp.Agent | Assigned, Intention )`
* `P( Transp.Team | Assigned, Transp.Agent )`
* `P( Deposited | Transp.Team, Intention )`

The exact values of these distributions are learned from the `history` of the agent, that consists of a table where are recorded, at each time-step, the values of the respective variables _in that time-step_. More precisely, the `ID` variables (denoted by initial uppercase letters) have associated `history` variables (with initial lowercase letters). At time-step `t` the `t`-th row of the `history` is defined by the values (boolean in non-decision nodes and actions in the decision node) of the `history` variables (`detected`, `assigned`, _etc._) and the values of th `ID` variables (`Detected`, `Assigned`, _etc._) are computed looking ahead the next `NF` values of the associated `history` variable.

## Algorithm

Given a `history` and an `ID`, the time-step deliberation procedure for the intention selection simply updates the `history` record, uses it to update the `ID` variables values and distributions and chooses an action according to the _Maximum Expected Utility_ principle.

```
def select_intention(history, ID, new_values):
    history.append( new_values )
    ID.append_values( history[now - NF : now] )
    ID.update_distributions() # MAP estimation of CPT's
    return ID.apply_meu()
```

For the "MAP estimation of CPT's" see

1.  R.G. Cowell, A. P. Dawid, S. L. Lauritzen, and D. J. Spiegelhalter. Probabilistic Networks and Expert Systems. Springer, 1999.
* M.Kearns, M. Littman, and S. Singh. Graphical models for game theory. In Proc. of the Conf. on Uncertainty in AI, 2001.
* R.Sutton and A. Barto. Reinforcment Learning: An Introduction. MIT Press, 1998.
* A.Ng and M. Jordan. PEGASUS: A policy search method for large MDPs and POMDPs. In Proc. of the Conf. on Uncertainty in AI, 2000.