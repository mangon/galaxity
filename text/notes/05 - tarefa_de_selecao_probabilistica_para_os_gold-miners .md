# Tarefa de selecção probabilística para o cenário "gold-miners"

**Objectivo**

Definir um agente ASL para o cenário `noisy-gold-miners` e em que pelo menos uma das funções de selecção usa uma ferramenta PGM/ML (como o `weka`) que aproveita um modelo do ruído nas acções e nas percepções de forma a superar o desempenho da equipa original, concebida para o cenário sem ruído.

**Passos**

1. Resolver as questões técnicas:
    
1. Descrever a interação com o ambiente
    * Descrever a base comum a todos os agentes
    * e um modelo do ruído

2. Procurar funções de **desempenho** que possam ser calculadas a partir desses elementos;

## 1. Questões técnicas

1. Configurar agente

    1. extende-se `jason.asSemantics.Agent` alterando os métodos `select*` e
    2. define-se o parâmetro `agentClass: …` no ficheiro `.mas2j` para a nova classe;

1. Adicionar ruído ao ambiente sem gerar imensas mensagens
    2. removendo o registo de uma excepção em `LocalMinerArch`

1. qual é o papel dos modelos (`arch.LocalWorldModel`) no comportamento dos agentes?


### Para que serve `arch.LocalWorldModel`?
Ainda não sei muito bem… Talvez seja necessário ler o livro (`RTFM`)

## 2. Interacção com o ambiente


> Afinal qual é exactamente o enunciado do problema `noisy-gold-miners`? Os agentes de uma equipa comunicam entre si? de borla???

### Base comum dos agentes **TODO: COMPLETE!**

* **percepções (crenças geradas pelo ambiente)**
    * **UPDT** `cell(X,Y,C)` conteúdo (`empty`, `enemy`, `ally`, `gold`, `obstacle`) na posição `X,Y`;
    * **UPDT** `pos(X,Y,T)` posição na grelha no instante `T`;
    * **UPDT** `carrying_gold(NG)` número de "ouros" transportados;
    * **UPDT** `container_has_space` se pode transportar mais "ouros";
    * **INIT** `depot(Golds,X,Y)` posição do depósito;
    * **INIT** `gsize(SX,SY)` dimensão da grelha;
    * **INIT** `steps(N)` passos disponíveis;
    * _more?_
    
* **crenças internas (geradas pelo processo de deliberação)** 
    * _?_
    
* **acções**
    * `drop`, `up`, `down`, `right`, `left`, `skip`, `pick`
    
* **capacidades**
    * `jia.direction(X,Y,TX,TY,D)` usa A* para encontrar caminho entre `X,Y` e `TX,TY`
    * `LocalWorldModel` a informação sobre o estado do mundo, influenciada pelas mensagens dos colegas;

### Modelos de ruído no ambiente

* **acções**
    * `Pr{ Executed(a) | Selected(a), Fatigue, NoiseAction } = NoiseAction ? … : Pr{ Executed(a) | Selected(a), Fatigue }`
* **percepções**
    * `container_has_space`
        * não tem ruído
    * `pos( PerPosX, PerPosY, PerPosStep )`
        * `Pr{ PerPos* | RealPos*, NoisePos } = NoisePos ? N( RealPos*, VarPos ) : Delta#RealPos*`
    * `carrying_gold( PerGolds )`
        * `Pr{ PerGolds | RealGolds, NoiseGolds } = NoiseGolds ? N( RealGolds, VarGolds ) : Delta#RealGolds`
    * `cell( x, y, PerValue )` com `Value in [obstacle,gold,enemy,ally,empty]`
        * `Pr{ PerValue(x,y) | RealValue(x,y), NoiseCell } = NoiseCell ? U : Delta#RealValue(x,y)`
       
<span style="color:red">Acabei de definir uma forma de influência do nível simbólico no estatístico.</span>    

## 3. Função de desempenho

O candidato evidente é a **pontuação da equipa**, conforme avaliado pela competição onde o cenário foi usado. Isto é: **o número de ouros entregues no depósito** durante o tempo permitido.

Para integrar a função de desempenho num diagrama de influência é preciso:

1. determinar que **aspectos do estado do ambiente** contribuem para mudar a função de desempenho;
2. determinar a **influência de cada acção** nesses aspectos


### 1. Aspectos do estado do ambiente
que podem (devem) contribuir para avaliar a utilidade das acções.

* ouros
    * detectados
    * ocultos
    * transportados (pela equipa)
    * transportados (pelo agente)
    * atribuídos
    * depositados
* percentagem de território conhecida
* menor distância de território desconhecido ao depósito 
* distância média (total?) dos ouros ao depósito
* distância do agente ao depósito
* cansaço
* tempo disponível

#### Diagramas de influência

    Legenda:
        DG: detected golds        HG: hidden golds
        TG: transp. golds (team)  AG: transp. golds (agent)
        SG: assigned golds        KG: deposited golds    
    
    Diagrama (ouros):    
        Pr{ BG | DG }
        Pr{ HG | DG }
        Pr{ AG | SG }
        Pr{ TG | SG, AG }
        Pr{ KG | TG }

Este diagrama descreve o processamento dos ouros:

1. descoberta
2. atribuição
3. transporte
4. depósito

Os outros aspectos referidos acima, e as acções, devem "adornar" este diagrama.



### 2. Efeito de cada acção
É necessário detalhar como cada acção afecta cada um dos aspectos anteriores, e eventualmente, como eles se influenciam entre si. Para isso o domínio de cada variável aleatória (subentendida em cada "aspecto") tem de estar definido.