\documentclass[17pt]{extarticle}

\usepackage{tikz}			% diagrams
\usetikzlibrary{calc}	% calculations within diagrams

\usepackage{amsmath}	% more math mode commands
\usepackage{amsfonts}

\usepackage{acronym} % acronyms...

\usepackage{geometry}
\geometry{
	a4paper,
	lmargin = 1cm,
	rmargin = 1cm,
	tmargin = 1cm,
	bmargin = 1.5cm
}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\algrenewcommand\algorithmicrequire{\textbf{Input:}}
\algrenewcommand\algorithmicensure{\textbf{Output:}}

\input{matdef}
\input{pgmdef}

%
%	NOTATION
%
\newcommand{\cte}[1]{\ensuremath{\mathtt{#1}}}
\newcommand{\cn}[1]{\ensuremath{\mathcal{#1}}}
\newcommand{\fn}[1]{\ensuremath{\textup{#1}}}
\newcommand{\setof}[1]{\ensuremath{\mathcal{#1}}}

%	ENVIRONMENT STATES
\newcommand{\pempty}{\cte{e}}
\newcommand{\pminer}{\cte{m}}
\newcommand{\pgold}{\cte{g}}
\newcommand{\pobstacle}{\cte{o}}
\newcommand{\envdom}{\setof{E}}

%	MINER ACTIONS
\newcommand{\aup}{\cte{u}}
\newcommand{\adown}{\cte{d}}
\newcommand{\aleft}{\cte{l}}
\newcommand{\aright}{\cte{r}}
\newcommand{\apick}{\cte{p}}
\newcommand{\adrop}{\cte{q}}
\newcommand{\askip}{\cte{s}}
\newcommand{\actiondom}{\setof{A}}

%	STATE & MINER

\newcommand{\neig}{\cn{N}}
\newcommand{\controls}{\cn{U}}
\newcommand{\states}{\cn{X}}
\newcommand{\observations}{\cn{Y}}

%	STATISTICS
\newcommand{\uniform}[1]{\cn{U}_{#1}}
\newcommand{\normal}[1]{\cn{N}_{#1}}
\newcommand{\prob}{\fn{P}}
\newcommand{\pa}{\fn{pa}}
\newcommand{\probs}{\fn{p}}
\newcommand{\bigo}{\cn{O}}

\newcommand{\given}{\middle|}

\newcommand{\msum}[2]{\sum\left[ #1\:\middle|\:#2 \right]}

\newcommand{\note}[1]{{\color{blue}#1}}


\acrodef{DBN}{Dynamical Bayesian Network}
\acrodef{HMM}{Hidden Markov Model}
\acrodef{TTBN}[2TBN]{Two-Time Bayesian Network}
\acrodef{MAP}{\emph{Maximum A Posteriori}}

\title{Perception correction in gold miners}
\author{Francisco Coelho}

\begin{document}
	\maketitle

	\begin{abstract}
		\note{The abstract text.}
	\end{abstract}

	\section{Introduction}

	\note{General problem intro.}

	\note{Techniques and results outline.}

 	\subsection{Basic assumptions}

 	It is important to define exactly what environment evolution can take place ``in-between'' time-steps. For example, if a time $t$ a miner leaves a location that location must be empty at time $t+1$ or can if be occupied by another miner or a gold? The simplest assumption is that \emph{no processing occurs between time-steps}. While this assumption simplifies the computation of transition probabilities, it also introduces a complication: How to solve clashes? For example, an empty location has a miner nearby that moves in and a gold generated in that location. Which is valid? This can be solved by sorting the agents, or randomly selection one case but such solutions must also be present in the transition models.

 	Another problem concerns the ``pick/drop'' actions. Exactly when can a miner pick or drop a gold and what happens to the environment? A solution, that avoids the multiple entities on the same location, assumes that ``pick/drop'' works on adjacent locations. This avoids the multiple entities problem but raises indeterminism: if there is more than one option to ``pick/drop'', which is carried? Again a solution can be defined by selecting uniformly at random one of the possibilities. Another (minor) advantage of this assumption is that the content of the actual miner's location is exactly ``miner'' and when it moves away that location becomes ``empty''.

 
 	\section{Problem statement}

	\begin{figure}[t]
		\begin{center}
			\begin{tabular}{ccc}
				\begin{tikzpicture}
				\node[var] (X) at (0,4) {$X$};
				\node[var] (X1) at (4,4) {$X'$};
				\node[var] (S1) at (4,2) {$S'$};
				\node[var,obs] (Y1) at (4,0) {$Y'$};
				\node[action,obs] (A) at (1,1) {$A$};			
				\node[action] (B) at (2,3) {$B$};

				\draw[->, >=latex]
					(X) edge (X1)
					(X1) edge (S1)
					(S1) edge (Y1)
					(A) edge (B)
					(B) edge (X1)
				;
				\end{tikzpicture}
				&				
				\begin{tikzpicture}
				\node[var] (S) at (0,4) {$S$};
				\node[var] (S1) at (4,4) {$S'$};
				\node[var,obs] (Y1) at (4,0) {$Y'$};
				\node[action,obs] (A) at (1,1) {$A$};			
				\node[action] (B) at (2,3) {$B$};

				\draw[->, >=latex]
					(S) edge (S1)
					(S1) edge (Y1)
					(A) edge (B)
					(B) edge (S1)
				;
				\end{tikzpicture}
				&				
				\begin{tikzpicture}
				\node[var] (X) at (0,4) {$S$};
				\node[var] (X1) at (4,4) {$S'$};
				\node[var,obs] (Y1) at (4,2) {$Y'$};

				\draw[->, >=latex]
					(X) edge (X1)
					(X1) edge (Y1)
				;

				\node[action,obs] (Plate) at (3,-0.2) {$B=b$};
				\draw[thick] ($(Plate.south west)$) rectangle ($(X1.north east) + (1.5, 0.5)$);
				\end{tikzpicture}		
				\\				
				(a) Environment \acs{TTBN} & (b) Scan grid \acs{TTBN} & (c) Scan grid \acs{DBN} given $B=b$
			\end{tabular}
		\end{center}
		\caption{Three perception models were a selected action, $A$, executed as $B$ drives the environment state from $X$ to $X'$. There the agent scan of ``his'' new sector, $S'$, is $Y'$. Diagram (a) depicts a miner placed in the environment, (b) focus the miner's knowledge and (c) reformulates the (b) as a \ac{DBN}.
		In this notation environment states are $X,X' = \SET{X_1,X'_1, \ldots}$, current sector $S' = S'_{1:9}$, scanner readings $Y' = Y'_{1:9}$, selected action $A$ and executed action $B$. Domains are $X_i, X'_i, S'_i, Y'_i \in \envdom = \SET{\pminer, \pobstacle, \pempty, \pgold}$ (miner, obstacle, empty and gold) and $A,B \in \actiondom = \SET{\askip, \apick, \adrop, \aup, \aright, \adown, \aleft}$ (skip, pick, drop, up, right, down, left).}
		\label{fig:two-trans-models}
	\end{figure}

	\subsection{Formalization}

	\note{Problem statement introductory text.}

	The \ac{TTBN} of figure~\ref{fig:two-trans-models}(a) represents the problem addressed here with environment states represented by $X,X'$. In figure~\ref{fig:two-trans-models}(b) the hidden states are only the scanned grid. At last figure~\ref{fig:two-trans-models}(c)  represents the problem as a \ac{DBN} plate indexed by actions. For that network the joint distribution is
	\begin{eqnarray}
		\prob\at{S,S',A,B,Y'}	&=&
			\prob\at{Y' \given S'}
			\prob\at{S' \given S, B}
			\prob\at{S}
			\prob\at{B \given A}
			\prob\at{A}\label{eq:network-decomp}
	\end{eqnarray}

	The formulation of this problem as a DBN benefits from treating the action ``error'', $\prob\at{B\given A}$ appart. Supposing that for each action $b\in B$ is defined a \ac{DBN} with sensor model  $\prob_b\at{Y' \given S'}$ and (sector) transition $\prob_b\at{S' \given S}$. Furthermore, let $N\at{s}$ be the current belief about the sensor reading. The problem now is to update the belief $N\at{s'}$ given the observation $Y'=y'$. From the network structure in equation~\ref{eq:network-decomp} 




	Supposing that the miner knows the selected action, $A = a$, the previous (estimated) sector state, $S = s$, and the current scanner readings, $Y' = y'$, the ``best'' estimate of the current sector state, $\hat{s}'$, is the \ac{MAP}
	\begin{eqnarray}
		\hat{s}' &=& \arg_{s'}\max \probs\at{s' \given s, a, y'}
	\end{eqnarray}

	From
	\begin{eqnarray}
		\nonumber	\prob\at{S' \given S, A, Y'}	&=& \frac{\prob\at{S', S, A, Y'}}{\prob\at{S,A,Y'}}
		\\
		\nonumber	&=& \frac{\sum_{b \in \actiondom}\prob\at{S', S, A, b, Y'}}{\sum_{b\in\actiondom}\sum_{s' \in \envdom}\prob\at{S, s', A, b, Y'}}
		\\
		\nonumber	&=& \prob\at{Y' \given S'}\sum_{b \in \actiondom}\frac{
							\prob\at{S' \given S, b}
							}{
							\sum_{s' \in \envdom}								
							 \prob\at{Y' \given s'}\prob\at{s' \given S, b}
							}
		\\
		\nonumber &=& \prob\at{Y' \given S'}\sum_{s' \in \envdom}\frac{1}{\prob\at{Y' \given s'}}	\sum_{b \in \actiondom}\frac{
							\prob\at{S' \given S, b}
							}{							
							 \prob\at{s' \given S, b}
							}
		\\
		\nonumber &=& \prob\at{Y' \given S'}\sum_{s' \in \envdom}\frac{1}{\prob\at{Y' \given s'}}	\sum_{b \in \actiondom}\frac{
							\prob\at{S',b \given S}\prob\at{b}
							}{							
							 \prob\at{s',b \given S}\prob\at{b}
							}
		\\
		\nonumber &=& \prob\at{Y' \given S'}\sum_{s' \in \envdom}\frac{1}{\prob\at{Y' \given s'}}	\sum_{b \in \actiondom}\frac{
							\prob\at{b \given S}\prob\at{S' \given S}
							}{							
							 \prob\at{b \given S}\prob\at{s' \given S}
							}
	\end{eqnarray}
	results (replacing the $s'$ above by $r'$)
	\begin{eqnarray}
		\probs\at{s' \given s, a, y'} &=& \probs\at{y' \given s'}\sum_{b \in \actiondom}\frac{
							\probs\at{s' \given s, b}
							}{
							\sum_{r' \in \envdom}								
							 \probs\at{y' \given r'}\probs\at{r' \given s, b}
							}
	\end{eqnarray}

	The sensor model assumes that the a cell reading is right with probability $\theta_1$ and, if wrong, the value is uniformly distributed over the remaining cases. This assumption can be written as
	\begin{eqnarray}
		\probs\at{y' \given s'} &=& \prod_{i=1:8} \theta_1\delta_{y'_i = s'_i} + \frac{1-\theta_1}{3}\delta_{y'_i \not= s'_i}
	\end{eqnarray}

	\subsection{Sector transition}
	
	The sector transition model, $\prob\at{S'\given S, B}$, represents the subjective understanding each miner has of the ``workings'' of his environment and that model can range from the very na\"{i}ve (\emph{e.g.} $\prob\at{S'_i = \pempty \given S, B} = \prob\at{S'_i = \pempty} = 1$) to the most informed (\emph{e.g.} including sophisticated miner motion or terrain models, information from allies, memory of older scans, \emph{etc.}).

	For the sake of completeness here is described a simple sector transition model based in the (plain wrong) assumptions that: 
	\begin{itemize}
		\item Inside the current sector cell content changes only by action of the miner;
		\item Outside the current sector cell contents are uniformly distributed.
	\end{itemize}
	

	\begin{figure}[t]
		\begin{center}
			\begin{tabular}{cc}
				{\bf Sector before action \aup} & {\bf Sector after action \aup}\\
				\begin{tikzpicture}[baseline=(current bounding box.center)]
					\begin{scope}[shift = {(-0.5, -0.5)}]
						\draw (0,0) grid (3,4);					
						\draw[fill = black!10] (1,1) rectangle (2,2);
					\end{scope}				
					\node at (0,0) {$S_7$};
					\node at (0,1) {$S_4$};
					\node at (0,2) {$S_1$};
					\node at (1,0) {$S_8$};
					\node at (1,1) {$S_5$};
					\node at (1,2) {$S_2$};
					\node at (2,0) {$S_9$};
					\node at (2,1) {$S_6$};
					\node at (2,2) {$S_3$};
				\end{tikzpicture}
				&
				\begin{tikzpicture}[baseline=(current bounding box.center)]
					\begin{scope}[shift = {(-0.5, -0.5)}]
						\draw (0,0) grid (3,4);		
						\draw[fill = black!10] (1,2) rectangle (2,3);			
					\end{scope}
					\node at (0,1) {$S'_7$};
					\node at (0,2) {$S'_4$};
					\node at (0,3) {$S'_1$};
					\node at (1,1) {$S'_8$};
					\node at (1,2) {$S'_5$};
					\node at (1,3) {$S'_2$};
					\node at (2,1) {$S'_9$};
					\node at (2,2) {$S'_6$};
					\node at (2,3) {$S'_3$};
				\end{tikzpicture}
			\end{tabular}
		\end{center}
		\caption{Moving a sector. This example shows the effect of moving ``up'' (\aup). The cell initially scanned by $S_1$ becomes scanned by $S'_4$. New terrain is scanned by the ``fringe'' $S'_1, S'_2, S'_3$ and the miner is in the shaded cells.}\label{fig:grid-transition}		
	\end{figure}

	Transitions of each sector location have three representative cases:
	\begin{description}
		\item[corner location] (\cte{NW}, \cte{NE}, \cte{SE}, \cte{SW}) represented by $S_1$ in table \ref{tab:corner-trans};
		\item[middle location] (\cte{N}, \cte{E}, \cte{S}, \cte{W}) represented by $S_2$ in table \ref{tab:middle-trans};
		\item[center location] the central sensor, $S_5$, always reports $\pminer$ thus can be discarded.
	\end{description}
	The remaining cases result from these by rotation. Columns in tables \ref{tab:corner-trans} and \ref{tab:middle-trans} include a boolean variable, $OK$, that resumes the relevant information in the full sector $S=S_{1:9}$. In particular $OK = 1$ iff the action can execute in the current sector state (see equation \ref{eq:ok}). Additional values that occur in table~\ref{tab:middle-trans}, $n_G$ and $n_E$, count the number of ``gold'' and (resp.) ``empty'' in locations  $S_2, S_4, S_6, S_8$ (that affect drop, pick and motion action).

	\begin{eqnarray}
		\nonumber OK &\Leftrightarrow&  b = \askip
		\\
		\nonumber	&\vee& b=\apick \wedge \pgold \in \SET{s_2, s_4, s_6, s_8}
		\\ 
		\nonumber	&\vee& b = \adrop \wedge \pempty \in \SET{s_2, s_4, s_6, s_8}
		\\
		\nonumber	&\vee& \PARC{b = \aup \wedge s_2 = \pempty} \vee \PARC{b = \aright \wedge s_4 = \pempty}
		\\
					&\vee& \PARC{b = \adown \wedge s_8 = \pempty} \vee \PARC{b = \aleft \wedge s_6 = \pempty}  \label{eq:ok}
	\end{eqnarray}

	\begin{table}[b]
		\caption{\small Transition probabilities structure for the $\text{NW} = S_1$ corner. Neighbor locations are $S_2$ (to the east) and $S_4$ (to south). The executed action is $B$.}\label{tab:corner-trans}
		\begin{center}
			$$
			\MAT{c|c||ccccc}{
			3584	& 4 	& 7 	& 4 	& 4 	& 4 	& 2 \\\hline
			\prob 	& S'_1 	& B 	& S_1 	& S_2 	& S_4 	& OK \\ \hline\hline
			1 		& x 	& \askip& x \\ \hline
			1 		& x 	& \apick& x \\ \hline
			1 		& x 	& \adrop& x \\ \hline
			1 		& x 	& \aup 	& x 	&		&		& 0\\
			0.25	& 	 	& \aup 	&   	&  		&		& 1\\ \hline
			1 		& x 	& \aright& x 	&		&		& 0\\
			1	 	& x 	& \aright&   	& x		&  		& 1\\ \hline
			1 		& x 	& \adown& x 	&		&		& 0\\
			1	 	& x 	& \adown&   	&  		& x		& 1\\ \hline
			1 		& x 	& \aleft& x 	&		&		& 0\\
			0.25	&   	& \aleft&   	& 		&  		& 1
			}
			$$
		\end{center}
	\end{table}
	\begin{table}[b]
		\caption{\small Transition probabilities structure for the $\text{N} = S_2$ location. Neighbor locations are $S_1$ (to the west) and $S_3$ (to the east). The executed action is $B$.}\label{tab:middle-trans}
		\begin{center}
			$$
			\MAT{c|c||ccccc}{
			3584	& 4		& 7 	& 4		& 4		& 4		& 2 \\ \hline
			\prob 	& S'_2 	& B 	& S_2 	& S_1 	& S_3	& OK \\ \hline\hline
			1 		& x 	& \askip& x \\ \hline
			1 		& x 	& \apick& x 	&		&		& 0 \\		
			\frac{1}{n_G} 	&\pempty& \apick& \pgold&		& & 1 \\	
			1-\frac{1}{n_G} 	&\pgold& \apick& \pgold&		& & 1 \\		
			1 		&x& \apick& x \not=\pgold&		&		& 1 \\  \hline
			1 		& x 	& \adrop& x 	&		&		& 0 \\ 	
			\frac{1}{n_E} 	&\pgold& \adrop& \pempty&		& & 1 \\
			1-\frac{1}{n_E} 	&\pempty& \adrop& \pempty&		& & 1 \\		
			1 		&x& \adrop& x \not=\pempty&		&		& 1 \\  \hline
			1 		& x 	& \aup 	& x 	&		&		& 0\\
			0.25 	&  		& \aup 	&		& 		&		& 1\\ \hline
			1 		& x 	&\aright& x 	&		&		& 0\\
			1 		& x 	&\aright&   	&  		& x		& 1\\ \hline
			1 		& x 	& \adown& x 	&		&		& 0\\
			1		&\pempty& \adown&   	& 		&  		& 1\\ \hline
			1 		& x 	& \aleft& x 	&		&		& 0\\
			1 		& x 	& \aleft&   	& x		& 		& 1
			}
			$$
		\end{center}
	\end{table}

	\section{Resolution}

	Exact resolution by \ac{HMM} filtering (in algorithm \ref{fig:hmm-filtering}) is not possible due state-space dimension ($2^{16}$ states imply $2^{32}$ parameters in the transition matrix, for each action). The structure aware methods of \acp{DBN} are required.

	\subsection{\ac{HMM} filters}

	The perception correction problem is a \ac{HMM} filtering task (see figure \ref{fig:hmm-filtering}). Here is outlined the resolution of this task. 

	\begin{figure}[t]
		\begin{center}
			\begin{tikzpicture}
				\node[action,obs] (U0) at (0,4) {$U_0$};
				\node[var] (X0) at (0,2) {$X_0$};
				\node[var,obs] (Y0) at (0,0) {$Y_0$};
				\node[action,obs] (Ut) at (4,4) {$U_t$};
				\node[var] (Xt) at (4,2) {$X_t$};
				\node[var,obs] (Yt) at (4,0) {$Y_t$};
				\node[action,obs] (Ut1) at (6,4) {$U_{t+1}$};
				\node[var] (Xt1) at (6,2) {$X_{t+1}$};
				\node[var,obs] (Yt1) at (6,0) {$Y_{t+1}$};
				\node (Xinf) at (8,2) {};

				\draw[->, >=latex]
					(U0) edge (X0)
					(X0) edge (Y0)					
					(X0) edge[dashed] (Xt)

					(Ut) edge (Xt)
					(Xt) edge (Yt)					
					(Xt) edge (Xt1)

					(Ut1) edge (Xt1)
					(Xt1) edge (Yt1)

					(Xt1) edge[dashed] (Xinf)
					;
			\end{tikzpicture}
		\end{center}
		\caption{\ac{HMM} filtering: given an estimate of the initial estate, $\hat{X}_0$, and sequences of controls, $u_{0:t}$, and observations, $y_{0:t}$, estimate the current system state, $\hat{X}_t$.}\label{fig:hmm-filtering}
	\end{figure}

	\begin{algorithm}[t]
		\caption{The \ac{HMM} filter task can be computed using the \textsc{Forward} function. A belief state is a distribution over $\states$, $u\in\controls$ and $y\in\observations$.}\label{alg:forward}
		\begin{algorithmic}
			\Require{current belief state ($b$),  next control ($u$) and next observation ($y$).}
			\Ensure{exact estimate of the next state.}
			\Function{Forward}{$b, u, y$}
				\ForAll{$x' \in \states$}
					\State $p\at{x'} \gets \sum_{x\in\states} T\at{x',x,u}b\at{x}$ \Comment{Predict step.}
					\State $b'\at{x'} \gets M\at{x',y} p\at{x'}$ \Comment{Update step.}
				\EndFor
				\State \Return $b' / \NORM{b'}$ \Comment{Normalize result.}
			\EndFunction				
		\end{algorithmic}
	\end{algorithm}

	The exact solution of the filtering task can be found by application of the \textsc{Forward} algorithm (Algorithm~\ref{alg:forward}). More specifically an \ac{HMM} is defined by sets of controls, states and observations, $\controls, \states, \observations$ (resp.) plus transition and observations distributions, $T\at{x',x,u} = \prob\at{X_{t+1} = x' \given X_{t} = x, U_{t} = t} $ and $M\at{x,y} = \prob\at{Y = y \given X = x} $. If $\controls, \states, \observations$ are finite sets the distributions can be described as matrices
	\begin{eqnarray}
		T\at{x',x,u} &=& T_{u;x',x} \\
		M\at{x,y} &=& M_{x,y}
	\end{eqnarray}
	and the \textsc{Forward} algorithm admits the compact formulation
	\begin{eqnarray}
		b' &\propto& M_{y} \odot T_{u}b\label{eq:forward}
	\end{eqnarray}
	where $b$ and $b'$ are distributions over $\states$, $u\in\controls, y\in\observations$, $M_y$ is the column that correspond to the observation $y$, $T_u$ is the $\states \to \states$ transition defined by control $u$ and ``$\odot$'' denotes the Hadamard (element-wise) product of vectors.

	{\bf Complexity.} The algebraic formulation of the exact estimation shows the need of approximate methods for non trivial cases. Supposing that $\SIZE{\states} = n$ for each action $u$ the respective transition matrix $T_u$ has $n^2$ entries. Furthermore, the computation of equation~\ref{eq:forward} entails $\bigo\at{n^3}$ products. In the gold miners scenario there are $n = 4^8 = 65536$ states so each forward computations involves at least $281474976710656 \approx 2.8\times 10^{14}$ products that take, at least, $1.4\times 10^5$ seconds (more than a day) using a $2\text{GHz}$ processor. Furthermore seven transition matrices of type $65536 \times 65536$ would be needed. If each float takes $64$ bits then these matrices occupy $229376$ gigabytes. Approximate methods and the state transition (bayesian) factorization provide an alternative to these costs. 

	\subsection{Particle Filters and Dynamical Bayesian Networks}
	
	\subsubsection{MCMC samples}

	Sampling is a key step for approximation methods. Formally, an approximation of $\prob\at{X\given e}$ by generating samples is the frequentist ratio
	\begin{eqnarray}
		\prob\at{X = x \given e} &\approx& \frac{N\at{x}}{N}
	\end{eqnarray}
	where $N\at{x}$ is the number of times $x$ is generated and $N$ is the total number of samples. The variance of the error of this approximation is proportional to $\frac{1}{\sqrt{N}}$.

	The major problem to be solved in a sampling process is the selection of samples that meet the evidence $e$. The MCMC method (when applied to bayesian networks), detailed in algorithm~\ref{alg:mcmc}, solves this problem by generating only samples compatible with $e$ and weighting each sample.


	\begin{algorithm}[t]
		\caption{MCMC sampling in a topologically sorted bayesian network to approximate a conditional $\prob\at{X\given E = e}$. }\label{alg:mcmc}
		\begin{algorithmic}
			\Require{bayesian network ($b$), evidence ($E = e$) and number of samples ($n$).}
			\Ensure{$n$ samples and weights of $\prob\at{X \given E = e}$ ($X$ are the non-evidence variables).}
			\Function{MCMC}{$b, E, n$}
				\For{$i \in 1:n$}\Comment{Generate $n$ samples.}
					\State $w_i \gets 1$ \Comment{Weight of this sample.}
					\For{$X_k \in \fn{nodes}\at{b}$}\Comment{Nodes in $b$ are topologically sorted.}
						\If{$X_k = E_j$} \Comment{If $X_k$ is the $j$th observed variable\ldots}						
							\State $x \gets e_j$ \Comment{Observed value.}
							\State $w_i \gets w_i\prob\at{X_k = x\given s_i}$ \Comment{Update sample weight.}
						\Else\Comment{If $X_k$ is not observed\ldots}
							\State $x \sim \prob\at{X_k \given s_i}$ \Comment{Draw a random value from $\prob\at{X_k \given s_i}$.}
						\EndIf

					\State $s_i \gets s_i \oplus x$ \Comment{Append current value to sample.}
					\EndFor

				\EndFor
				\State \Return $s,w$ 
			\EndFunction				
		\end{algorithmic}
	\end{algorithm}

	The algorithm \ref{alg:pf} provides the update function for a particle filter on a DBN but complexity problems persist. In particular, the size of factor $\prob_b\at{X' \given X}$ is $\bigo\at{\SIZE{X}^2}$. However, in the context of the problem addressed here an estimate $\hat{x}$ of $X$ is know: the sample size is $1$. Replacing $\prob_b\at{X' \given X}$ by $\prob_b\at{X' \given X = \hat{x}}$ reduces the size of the factor to $\bigo\at{\SIZE{X}}$.
	\begin{algorithm}[t]
			\caption{Update Particle Filter to estimate $\prob\at{X' \given Y'}$ where $Y'$ are the observed variables and $X,X'$ the hidden variables. This algorithm assumes that a given a bayesian network $n$ defines the transition $\prob_n\at{X' \given X}$ and likelihood $\prob_n\at{Y' \given X'}$ distributions.}\label{alg:pf}
			\begin{algorithmic}
				\Require{bayesian network $n$, previous estimate $N\at{X}$ and evidence $Y' = y'$.}
				\Ensure{updated $N\at{X'}$.}
				\Function{UpdateParticleFilter}{$N\at{X}, y, n$}
					\State $F\at{X'} \gets \Call{Marginal}{\prob_n\at{X' \given X}N\at{X},X}$ \Comment{forward step.}
					\State $W\at{X'} \gets \prob_n\at{Y' = y' \given X'}F\at{X'}$ \Comment{weight step.}
					\State $N\at{X'} \gets \SIZE{N\at{X'}}\Call{Normalize}{W\at{X'}}$ \Comment{resample step.}
					\State \Return $N\at{X'}$
				\EndFunction				
			\end{algorithmic}
		\end{algorithm}
	% \begin{algorithm}[t]
	% 	\caption{Particle Filter in a topologically sorted dbn to generate a set of samples that approximate $\prob\at{X'}$. }\label{alg:pf}
	% 	\begin{algorithmic}
	% 		\Require{dbn ($b$) with prior $\prob\at{X}$, transition model $\prob\at{X'\given X}$ and observation model $\prob\at{E\given X}$, evidence ($E = e$) and samples of $\prob\at{X}$ ($s$).}
	% 		\Ensure{Samples of $\prob\at{X'}$.}
	% 		\Function{ParticleFilter}{$b, e, s$}
	% 			\For{$i \in 1:\SIZE{s}$}\Comment{Update $n = \SIZE{s}$ samples.}
	% 				\State $s_i \sim \prob\at{X' \given X = s_i}$ \Comment{Draw a sample from next state.}
	% 				\State $w_i \gets \prob\at{e \given X = s_i}$ \Comment{Weight by likelihood.}
	% 			\EndFor
	% 			\State $s \gets \Call{Resample}{s,w}$\Comment{Resample by weights.}
	% 			\State \Return $s$ 
	% 		\EndFunction				
	% 	\end{algorithmic}
	% \end{algorithm}

	\section{Other notes}

	\subsection{Implementation of sparse matrices}

	Sound and supported support for sparse matrices effectively doesn't exist:
	\begin{itemize}
		\item In BLAS/LAPACK progress seems to be towards a standard, not sound, tested implementations;
		\item In numpy there are a variety of classes for sparse matrices. Unfortunately, numpy is not compatible with java;
		\item Tried a custom ``dictionary of keys'' implementation, but performance is not enough for the problem in hands;
	\end{itemize}

	\appendix
	\section{Transition factors}

	\begin{itemize}
		\item If $OK = 0$, for all $i=1:8, b\in\controls$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_i}$ where
	\begin{eqnarray}
		\fn{copy}\at{A,B} &=& \PARR{\begin{matrix}
			A & B & \prob \\
			\hline
			\pminer & \pminer & 1.0 \\
			\pobstacle & \pobstacle & 1.0 \\
			\pempty & \pempty & 1.0 \\
			\pgold & \pgold & 1.0 \\
		\end{matrix}}
	\end{eqnarray}
		\item If $OK = 1$:
		\begin{itemize}
			\item If $b=\askip$ for all $i = 1:8$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_i}$;

			\item If $b=\apick$
				\begin{itemize}
					\item If $i=1,3,7,9$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_i}$;

					\item If $i = 2,4,6,8$ then $\prob\at{S'_i \given S}$ is $F_2\at{S'_i,S_i, \frac{1}{n_G}}$ where
						\begin{eqnarray}
							F_2\at{A,B,x} &=& \PARR{\begin{matrix}
							A & B & \prob \\
								\hline
							\pempty & \pgold & x \\
							\pgold & \pgold & 1-x \\
								\pminer & \pminer & 1.0 \\
								\pobstacle & \pobstacle & 1.0 \\
								\pempty & \pempty & 1.0
							\end{matrix}}
						\end{eqnarray}
				\end{itemize}

				\item  If $b=\adrop$
				\begin{itemize}
					\item If $i=1,3,7,9$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_i}$;
					\item If $i = 2,4,6,8$ then $\prob\at{S'_i \given S}$ is $F_3\at{S'_i,S_i, \frac{1}{n_E}}$ where
						\begin{eqnarray}
							F_3\at{A,B,x} &=& \PARR{\begin{matrix}
							A & B & \prob \\
								\hline
							\pgold & \pempty & x\\
							\pempty & \pempty & 1-x \\
								\pminer & \pminer & 1.0 \\
								\pobstacle & \pobstacle & 1.0 \\
								\pgold & \pgold & 1.0
							\end{matrix}}
						\end{eqnarray}
				\end{itemize}

				\item If $b = \aup$,
					\begin{itemize}
						\item If $i = 1,2,3$ then $\prob\at{S'_i \given S}$ is  $\fn{uniform}\at{S'_i}$ where
							\begin{eqnarray}
								\fn{uniform}\at{A} &=& \PARR{
								\begin{matrix}
									A & \prob\\
									\hline
									\pminer & 1/4 \\
									\pobstacle & 1/4 \\
									\pempty & 1/4 \\
									\pgold & 1/4
								\end{matrix}
								}
							\end{eqnarray}
						
						\item If $i = 4,6,7,9$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_{i-3}}$;

						\item $\prob\at{S'_8 \given S}$ is $\fn{delta}\at{S'_8,\pempty}$ where
							\begin{eqnarray}
								\fn{delta}\at{A,x} &=& \PARR{
								\begin{matrix}
									A & \prob\\
									\hline
									x & 1 \\
								\end{matrix}
								}
							\end{eqnarray}

						\item If $b=\adown$,
							\begin{itemize}
								\item If $i = 7,8,9$ then $\prob\at{S'_i \given S}$ is $\fn{uniform}\at{S'_i}$;

								\item If $i = 1,3,4,6$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_{i+3}}$;

								\item$\prob\at{S'_2 \given S}$ is $\fn{delta}\at{S'_2, \pempty}$;
							\end{itemize}



						\item If $b=\aleft$,
							\begin{itemize}
								\item If $i = 1,4,7$ then $\prob\at{S'_i \given S}$ is $\fn{uniform}\at{S'_i}$;

								\item If $i = 2,3,8,9$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_{i-1}}$;

								\item$\prob\at{S'_6 \given S}$ is $\fn{delta}\at{S'_6, \pempty}$;
							\end{itemize}



						\item If $b=\aright$,
							\begin{itemize}
								\item If $i = 3,6,9$ then $\prob\at{S'_i \given S}$ is $\fn{uniform}\at{S'_i}$;

								\item If $i = 1,2,7,8$ then $\prob\at{S'_i \given S}$ is $\fn{copy}\at{S'_i,S_{i+1}}$;

								\item$\prob\at{S'_4 \given S}$ is $\fn{delta}\at{S'_4, \pempty}$;
							\end{itemize}
					\end{itemize}
		\end{itemize}
	\end{itemize}

	

	

	

	

	


\end{document}