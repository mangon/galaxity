# Galaxity: Probabilistic Selection in AgentSpeak(L)

**Abstract**  
Agent programming is mostly a symbolic artificial intelligence discipline and, as such, draws little benefits from sub-symbolic areas as machine learning and probabilistic graphical models. However, the greatest objective of agent research is the achievement of autonomy in dynamical and complex environments --- a goal that implies embracing uncertainty and therefore the entailed representations, algorithms and techniques.

## Introduction
Agent autonomy is a key objective in artificial intelligence (AI). Complex and dynamic environments, like the physical world where robots must delve, impose a degree of uncertainty  that challenges the vocation of symbolic processing. But while a statistical approach --- currently expressed in machine learning (ML) and probabilistic graphical models (PGM) --- is required for certain aspects of such tasks, a great deal of agent programming is better handled by declarative programming (_e.g._ `prolog`) and more specifically, belief-desire-intention (BDI) architectures for autonomous agents, that are part of symbolic (AI).

## Applications

* **Enhanced Personal Information Manager**
	- Manage contacts, appointments and goals
	- Propose tasks, shopping lists, _etc_
	- Find resources, references
	- Record progress, status
* **Vehicle Autonomous Driver**
	- Handle environment and way-points