\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{backgrounds}
\usepackage{acronym}

\input{pgmdef}
\input{matdef}

\acrodef{BRF}[BRF]{belief-revision function}
\acrodef{PCF}[PCF]{percept-correction function}
\acrodef{CPT}[CPT]{conditional probability table}

\tikzset{obs/.style={var, fill = black!10}}
\tikzset{rel/.style={fill = black!5}}


%
%	ACTIONS
%
\newcommand{\aup}{\ensuremath{\uparrow}}
\newcommand{\adown}{\ensuremath{\downarrow}}
\newcommand{\aleft}{\ensuremath{\leftarrow}}
\newcommand{\aright}{\ensuremath{\rightarrow}}
\newcommand{\apick}{\ensuremath{\Uparrow}}
\newcommand{\adrop}{\ensuremath{\Downarrow}}
\newcommand{\askip}{\ensuremath{\emptyset}}


%
%	PERCEPT VALUES
%
\newcommand{\pempty}{\ensuremath{\bigcirc}}
\newcommand{\pgold}{\ensuremath{\otimes}}
\newcommand{\pminer}{\ensuremath{\odot}}
\newcommand{\pobstacle}{\ensuremath{\oplus}}



%
% LOCAL TOPO DIAGRAMS
%


\newcommand{\loccorner}[1] {
		\node[var, rel] (C0) at (1,1) {$S$};
		\node[var] (C1) at (2,1) {$a$};
		\node[var] (C2) at (1,0) {$b$};
		\node[var] at (0,1) {};
		\node[var] at (1,2) {};
		\node at (1.5, 0.5) {#1};
		\path
			(C1) edge (C0)
			(C2) edge (C0)
		;
}

\newcommand{\locmiddle}[1] {
		\node[var, rel] (M0) at (1,1) {$S$};
		\node[var] (M1) at (2,1) {$a$};
		\node[var] (M2) at (1,0) {$b$};
		\node[var] (M3) at (0,1) {$c$};
		\node[var] at (1,2) {};
		\node at (1.5, 0.5) {#1};
		\path
			(M1) edge (M0)
			(M2) edge (M0)
			(M3) edge (M0)
		;				
}

\newcommand{\loccenter}[1] {
		\node[var, rel] (X0) at (1,1) {$S$};
		\node[var] (X1) at (2,1) {$a$};
		\node[var] (X2) at (1,0) {$b$};
		\node[var] (X3) at (0,1) {$c$};
		\node[var] (X4) at (1,2) {$d$};
		\node at (1.5, 0.5) {#1};
		\path
			(X1) edge (X0)
			(X2) edge (X0)
			(X3) edge (X0)
			(X4) edge (X0)
		;				
}

\newcommand{\sensorgrid}{
		\node[var,obs] (S0) at (0,2) {$Y_0$};
		\node[var,obs] (S1) at (1,2) {$Y_1$};
		\node[var,obs] (S2) at (2,2) {$Y_2$};
		\node[var,obs] (S3) at (0,1) {$Y_3$};
		\node[var,obs] (S4) at (1,1) {$Y_4$};
		\node[var,obs] (S5) at (2,1) {$Y_5$};
		\node[var,obs] (S6) at (0,0) {$Y_6$};
		\node[var,obs] (S7) at (1,0) {$Y_7$};
		\node[var,obs] (S8) at (2,0) {$Y_8$};

		\path[dotted]
			(S0) edge (S1)
			(S1) edge (S2)
			(S3) edge (S4)
			(S4) edge (S5)
			(S6) edge (S7)
			(S7) edge (S8)
			(S0) edge (S3)
			(S1) edge (S4)
			(S2) edge (S5)
			(S3) edge (S6)
			(S4) edge (S7)
			(S5) edge (S8)
		;				
}

%
% MATH
%

\newcommand{\PC}[2] {\ensuremath{P\at{#1\middle| #2}}}
\newcommand{\col}[1] {\ensuremath{\begin{array}[c]#1\end{array}}}

\title{Perception correction for BDI agents.}
\author{Francisco Coelho}

\begin{document}
\maketitle

\begin{figure}[t]
	\begin{center}
		\begin{tabular}{lr}	
		\begin{tikzpicture}
			\draw (0,0) grid (5,5);
			\draw[fill=black!25] (2,2) rectangle (3,3);
			\begin{scope}[shift = {(1.5, 1.5)}]
				\sensorgrid
			\end{scope}
		\end{tikzpicture}
		&	
		\begin{tikzpicture}
			\draw (0,0) grid (5,5);
			\draw[fill=black!25] (2,3) rectangle (3,4);
			\begin{scope}[shift = {(1.5, 2.5)}]
				\sensorgrid
			\end{scope}
		\end{tikzpicture}
		\\
		Before moving up. & After moving up.
			
		\end{tabular}
	\end{center}
	\caption{Sensor grid. The miner is in the location scanned by $Y_4$.}\label{sensor-grid}
\end{figure}

A (robotic) miner is equiped with a $3\times 3$ grid of sensors (figure \ref{sensor-grid}) that detects the contents of its neighborhood. Each sensor scans a position and reports the content, that can be one of empty, obstacle, gold or miner. The miner can also select an action to transform his immediate neighborhood. Action options are up, down, left, right, pick, drop and skip. See table \ref{notation} for a reference of perceptions and actions.

\begin{table}[t]
	\caption{Perceptions and actions in the gold miners environment.}\label{notation}
	\begin{center}
		\begin{tabular}{lcl}
			\bf{Name} & \bf{Symbol} & \bf{Meaning} \\ \hline
			empty & \pempty & The location is empty.\\
			obstacle &  \pobstacle & The location has an obstacle.\\
			gold &  \pgold & The location has a gold.\\
			miner &  \pminer & The location has a miner.\\ \hline
			up &  \aup & Moves the miner north.\\
			down &  \adown & Moves the miner south.\\
			left &  \aleft & Moves the miner west.\\
			right &  \aright & Moves the miner east.\\
			pick &  \apick & The miner picks a gold at its location.\\
			drop &  \adrop & The miner drops a gold at its location.\\
			skip &  \askip & No action.\\
		\end{tabular}
	\end{center}
\end{table}


The problem is that both sensors and actions are noisy and the symbolic inference process ineherent to BDI doesn't deal well with such scenarios. Perceptions are treated as (true) facts of the environment and, if ill detected, the deliberation process works on wrong assumptions.

A possibility to help in this situation without changing the deliberation engine is to correct the scanned percepts according to the current beliefs and a model of the environment.

Our proposal, sketched in figure \ref{include-pcr}, is to enter into the believe revision step of BDI and prepend a probabilistic \ac{PCF} step to take advantage of both the current beliefs and knowledge of the environment dynamics.

\begin{figure}[t]
	\begin{center}	
		\begin{tabular}{lr}
			\begin{tikzpicture}						
				\node at (-1,2) (ENV1) {Percepts};
				\node[draw, fill=black!5] at (1,2) (BRF) {BRF};
				\node[draw, rounded corners] at (2,3) (BB) {Beliefs};
				\draw[cond, ->] (ENV1) -> (BRF);
				\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
			\end{tikzpicture}
			&
			\begin{tikzpicture}		
				\node at (-1,2) (ENV1) {Percepts};
				\node[draw, fill = white] at (1,2) (PCF) {PCF};
				\node[draw, fill = white] at (3,2) (BRF) {(old) BRF};
				\node[draw, rounded corners] at (4,3) (BB) {Beliefs};
				\draw[cond, ->](ENV1) edge (PCF);
				\draw[cond, ->] (PCF) edge (BRF);
				\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
				\draw[cond,<->,rounded corners=3pt] (PCF) |- (BB);
				\begin{pgfonlayer}{background}				
					\draw[fill=black!5]
						($(PCF.north west)+(-0.25,-0.75)$)
						rectangle
						($(BRF.south east)+(0.25,0.85)$);	
				\end{pgfonlayer}
			\end{tikzpicture}
			\\			
			Detail current belief revision\ldots & \ldots to include perception correction.
		\end{tabular}
	\end{center}
	\caption{Inclusion of a percept-correction function before belief revision.}\label{include-pcr}
\end{figure}

\begin{figure}[t]
	\begin{center}
		\begin{tikzpicture}
		    \node[var] (X0) at (0,3) {$X^{t-1}_0$};
		    \node[var] (X8) at (1,2) {$X^{t-1}_8$};
		    \draw[dotted] (X0) -- (X8);

		    \node[var] (Z0) at (4,3) {$X^{t}_0$};
		    \node[var] (Z8) at (5,2) {$X^{t}_8$};
		    \draw[dotted] (Z0) -- (Z8);

		    \node[obs] (Y0) at (0,1) {$Y^{t-1}_0$};
		    \node[obs] (Y8) at (1,0) {$Y^{t-1}_8$};
		    \draw[dotted] (Y0) -- (Y8);

		    \node[obs] (W0) at (4,1) {$Y^{t}_0$};
		    \node[obs] (W8) at (5,0) {$Y^{t}_8$};
		    \draw[dotted] (W0) -- (W8);

		    \node[obs] (A) at (2,0.5) {$A^{t-1}$};
		    \node[var] (B) at (3,1.5) {$B^{t-1}$};

		    \path[->, every edge/.style={cond}]
		        (X0) edge (Z0)        
		        (X0) edge (Z8)
		        (X8) edge (Z0)
		        (X8) edge (Z8)
		        (X0) edge (Y0)
		        (X8) edge (Y8)
		        (Z0) edge (W0)
		        (Z8) edge (W8)
		        (A) edge (B)
		        (B) edge (Z0)
		        (B) edge (Z8)
		    ;
		\end{tikzpicture}
	\end{center}
	\caption{Network structure of the perception-correction problem. The previous state is $X^{t-1}$ and the miner selected action $A$. Since actions are noisy, $B$ is the action executed. The environment evolve to state $X^t$ that is scanned into $Y^t$. Nodes for hidden variables are clear and for observed variables shaded.}\label{network-structure}	
\end{figure}

The perception-correction problem can be stated as a probabilistic inference problem, the natural framework to deal with uncertainty. Probabilistic graphical models and, in particular, bayesian networks provide such framework. The structure of the percept-correction inference problem is depicted in Figure \ref{network-structure}. The task is to estimate the current (unknown) environment states $X^t_{0:8}$ given the previous estimate $X^{t-1}_{0:8}$, the selected transition $A$ and the current sensor readings $Y^t_{0:8}$. Since the transition is also subject to noise, the action really executed is $B$ (also unknown).

The sensor transition depends on the cell-state transition. Table \ref{loc-notation} defines notation to represent quantities useful to compute (in table \ref{transition-prob}) the probabilities of possible cell-state transitions. Without prior knowledge on other miners motion algorithms the most informed measures are the $\theta_2$ and $\theta_3$ parameteres defined in table \ref{loc-notation}.

\begin{table}[t]
	\caption{Notation to location transitions.}\label{loc-notation}
	\begin{center}
		\begin{tabular}{ll}
			\bf{Symbol} & \bf{Meaning}\\ \hline
			$\theta^N$ & probability that a new \pgold\ is generated in a \pempty.\\
			$\theta^E_m$ & probability that one of $m$ adjacent \pminer\ enters an \pempty.\\
			$\theta^G_m$ &  probability that one of $m$ adjacent \pminer\ enters a \pgold.\\
			$e$ & number of adjacent \pempty\ locations.\\
			$m$ & number of adjacent \pminer\ locations.\\
			$g$ & number of adjacent \pgold\ locations.\\
			$o$ & number of adjacent \pobstacle\ locations.\\
			$1-\Sigma$ & the complement to $1$ of the sum of remaining cases.\\
		\end{tabular}
	\end{center}
\end{table}

\begin{table}[t]
	\caption{Probabilities of possible transitions.}\label{transition-prob}
	\begin{center}
		\begin{tabular}{ccll}
			\bf{From}	& \bf{To}		& \bf{Probability} & \bf{Remarks} \\ \hline
			\pobstacle	& \pobstacle	& $1$ & Obstacles never change.\\ \hline
			\pempty		& \pempty		& $1-\Sigma$ \\
						& \pgold		& $\PARC{1-\theta^E_m}\theta^N$ & No adjacent miner enters\\
						&				&& and a gold is generated.\\
						& \pminer		& $\theta^E_m$ & An adjacent miner enters.\\ \hline
			\pgold		& \pgold		& $1-\Sigma$ \\
						& \pminer		& $\theta^G_m$ & An adjacent miner enters.\\ \hline
			\pminer		& \pempty		& $1-\Sigma$ \\ 
						& \pminer		& $1-\PARC{e\theta^E_1 + g\theta^G_1}\PARC{1-\theta^E_m}$
										& Either the miner stays ($1-\PARC{e\theta^E_1+g\theta^G_1}$)\\
						&				&& or the miner leaves and other enters.\\
						& \pgold		& $\theta^N\PARC{e\theta^E_1+g\theta^G_1}\PARC{1-\theta^E_m}$ & Miner leaves ($e\theta^E_1+g\theta^G_1$), \\
						&				&& no adjacent miner enters\\
						&				&& and a gold is generated.
						

			
		\end{tabular}
	\end{center}
\end{table}

For each sensor $X$ and action $B$ the \ac{CPT} for $\PC{X}{pa\at{X}, B}$ must be defined. The regularity of the sensor grid produces many symmetries that can be used to reduce the complexity. First, the local topology of each sensor has only three types, depicted in figure \ref{local-topology}.


\begin{figure}[t]
	\begin{center}
		\begin{tikzpicture}

			\begin{scope}[shift = {(0,0)}]
			\loccenter{(i)}
			\end{scope}

			\begin{scope}[shift = {(4,0)}]
			\locmiddle{(ii)}
			\end{scope}

			\begin{scope}[shift = {(8,0)}]
			\loccorner{(iii)}
			\end{scope}

		\end{tikzpicture}
		
	\end{center}
	\caption{Local topologies for the sensor nodes. The sensor is represented by $S$ and $a,b,c,d$ are the neighbours that also have sensors. Diagram (i) depicts an interior sensor, (ii) a sensor in the frontier but not a corner (iii).}\label{local-topology}
\end{figure}

In particular notice that:

\begin{enumerate}
	\item For corners:
	\begin{eqnarray*}
		\PC{X'_0}{X_0, X_3,X_1, \alpha_i} &=& \PC{X'_2}{X_2, X_1, X_5, \beta_i}\\
		&=& \PC{X'_8}{X_8, X_5, X_7, \gamma_i}\\
		&=& \PC{X'_6}{X_6, X_7, X_3, \delta_i}
	\end{eqnarray*}
	where
	\begin{eqnarray*}
		\alpha &=& \PARC{\aup,\aright,\adown,\aleft,\apick,\adrop,\askip}\\
		\beta &=& \PARC{\aright,\adown,\aleft,\aup,\apick,\adrop,\askip}\\
		\gamma &=& \PARC{\adown,\aleft,\aup,\aright,\apick,\adrop,\askip}\\
		\delta &=& \PARC{\aleft,\aup,\aright,\adown,\apick,\adrop,\askip}\\
	\end{eqnarray*}
	so that the \ac{CPT} of a single corner can be (almost) directly adapted to all the corneres;

	\item For the middle scanners the same pattern apply:
	\begin{eqnarray*}
		\PC{X'_1}{X_1,X_2,X_4,X_0,\alpha_i}&=& \PC{X'_5}{X_5, X_8, X_4, X_2\beta_i}\\
		&=& \PC{X'_7}{X_7, X_8, X_4, X_6, \gamma_i}\\
		&=& \PC{X'_3}{X_3, X_6, X_4, X_0, \delta_i}
	\end{eqnarray*}
	
	\item There is only one inner scanner so this kind of reduction doesn't apply; 
	
	\item The corner and middle \ac{CPT} can be obtained from the interior nodes by maginalisation (see below);
	
	\item For interior sensors only one of the motion actions (\aup, \aleft, \adown, \aright) needs to be defined --- others can be obtained by rotating the neighborhood;
	
	\item Still concerning interior sensors, for the non-motion actions (\askip, \apick, \adrop) the differences are minimal: consider the \ac{CPT} for \askip; supposing the miner is carrying golds, the \ac{CPT} for \adrop\ only differs on the cases where the current location is \pempty\ and the next state of that location is 
\end{enumerate}

With these reductions it suficces to define, for each action, the \ac{CPT} of a corner, a middle and the interior scanner. But this can be further simplified: The diagrams in figure \ref{local-topology} indicate that \emph{e.g.} for the middle $X_1$:

$$
 	\PC{X'_1}{X_1,X_2,X_4,X_0,\alpha_i} = \sum_{X_1} \PC{X'_4}{X_4,X_5,X_7,X_3,X_1, \alpha_i}
$$
and for the corner $X_0$:

$$
 	\PC{X'_0}{X_0,X_3,X_1,\alpha_i} = \sum_{X_1,X_3} \PC{X'_4}{X_4,X_5,X_7,X_3,X_1, \alpha_i}
$$
\end{document}
