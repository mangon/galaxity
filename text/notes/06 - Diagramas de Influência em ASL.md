# 06 - Suportar diagramas de influência no AgentSpeak(L)


## Bibliotecas Java para Diagramas de Influência

* [SamIam](http://reasoning.cs.ucla.edu/samiam/) ()
* [Banjo](http://www.cs.duke.edu/~amink/software/banjo/) (research)
* [BNJ](http://bnj.sourceforge.net) (&leq; 2004; research)
* [CIspace](http://150.214.140.135/~cgdiaz/CIspace/) (&leq; 2011?; teach)
* [Elvira](http://leo.ugr.es/elvira/) (&leq; 2001; spanish)
* ~~[JAGS](http://mcmc-jags.sourceforge.net) (&leq; 2013; R; BUGS)~~
* [RISO](http://riso.sourceforge.net) (&leq; 2006)
* [UnBBayes](http://unbbayes.sourceforge.net) (&leq; 2013; brasil; FOL; semweb!;++)
* [VIBES](http://vibes.sourceforge.net) (&leq; 2004;)
* [MALLET](http://mallet.cs.umass.edu/index.php) (&leq; 2013; loooks nice)

## _Short List_ de Bibliotecas

* [SamIam](http://reasoning.cs.ucla.edu/samiam/) (não tem ID---)
* [UnBBayes](http://unbbayes.sourceforge.net) (bugish)
* [MALLET](http://mallet.cs.umass.edu/index.php) (&leq; 2013; loooks nice)
* [OpenMarkov](http://www.openmarkov.org)
