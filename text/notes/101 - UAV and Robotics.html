<!DOCTYPE html><html><head><meta charset="utf-8"><script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<script>
MathJax.Hub.Config({
  config: ["MMLorHTML.js"],
  extensions: ["tex2jax.js"],
  jax: ["input/TeX"],
  tex2jax: {
    inlineMath: [ ['$','$'], ["\\(","\\)"] ],
    displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
    processEscapes: false
  },
  TeX: {
    extensions: ["AMSmath.js", "AMSsymbols.js"],
    TagSide: "right",
    TagIndent: ".8em",
    MultLineWidth: "85%",
    equationNumbers: {
      autoNumber: "AMS",
    },
    unicode: {
      fonts: "STIXGeneral,'Arial Unicode MS'"
    }
  },
  showProcessingMessages: false
});
</script>
<title>101 - UAV and Robotics</title></head><body><article class="markdown-body"><h1 id="uav-and-robotics"><a name="user-content-uav-and-robotics" href="#uav-and-robotics" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>UAV and Robotics</h1>
<h2 id="overview"><a name="user-content-overview" href="#overview" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>Overview</h2>
<p>We start by adopting a general model that defines a stack of layers:</p>
<ol>
<li>World + Hardware (Robot/Drone)</li>
<li>LL Control (basic actuators and sensor API)</li>
<li>HL Control (motion and sensors API)</li>
<li>State estimation (compute <code>p(s)</code>)</li>
<li>Deliberation (select an action <code>a</code> from state estimate <code>p(s)</code>)</li>
</ol>
<p>For example, </p>
<ol>
<li>World + Hardware = this world with our AR.Drone 2.0</li>
<li>LL Control = the stack that Parrot places in this hardware</li>
<li>HL Control = YADrone</li>
<li>State Estimation = WIP by fc</li>
<li>Deliberation = WIP by vbn</li>
</ol>
<p>We are interested in the <strong>State estimation</strong> and <strong>Deliberation</strong> levels and their interplay.</p>
<h2 id="state-estimation"><a name="user-content-state-estimation" href="#state-estimation" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>State estimation</h2>
<p>The task here is to get the feed from the previous level (<em>HL Control</em>) and produces a probabilistic current state estimation, <em>i.e.</em> compute the distribution <code>p(s') = p(s' | s, a)</code> where <code>s'</code> is the current state, <code>s</code> the preceeding state and <code>a</code> the last executed action.</p>
<p>For this task the feed from the level below (HL Software) has the following components:</p>
<ol>
<li>Video (front camera, lower camera)</li>
<li>Pose<ul>
<li>Attitude (pitch, roll, yaw)</li>
<li>Velocity (vx, vy, vz)</li>
<li>Altitude (z)</li>
</ul>
</li>
<li>Internal state<ul>
<li>Battery level</li>
<li>Mode (hovering, landed, emergency, <em>etc</em>)</li>
</ul>
</li>
<li><em>maybe, with the <strong>navigation</strong> module,</em> Global Position<ul>
<li>GPS coordinates (lat, long)</li>
</ul>
</li>
</ol>
<p>The level above (Deliberation) also produces a feed of commands that must be observed in order to enable state update.</p>
<h3 id="task-drone-in-a-box"><a name="user-content-task-drone-in-a-box" href="#task-drone-in-a-box" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>Task: Drone in a box</h3>
<blockquote>
<p>Suppose that the world is a box with only the drone inside it (<em>and that cows are perfect spheres ;-)</em>). At any given moment the state is a tuple <code>(pitch, roll, yaw, a, b, u, v, x, y)</code> where <code>pitch, roll, yaw</code> are given from the attitude sensors and <code>a, b, u, v, x, y</code> are the distances the center of the drone is from, resp., the front, back, top, bottom, left and right faces of the box.</p>
<p>The initial dimensions of the box are unknown and the faces are not static: <code>a+b</code>, <code>u+v</code> and <code>x+y</code> <strong>are not constants</strong>. Actions are <code>takeoff, land, hover, up, down, left, right, turnleft, turnright, incspeed, decspeed</code> and the sensors include the video, pose and internal state sets.</p>
<p>The drone starts in the floor (<code>v = 0</code>). <strong>Estimate the current state of the drone</strong>.</p>
</blockquote>
<p>For a first resolution of this task let&rsquo;s suppose that the <em>cube state</em> variables (<code>a,b,u,v,x,y</code>) are continuous with gaussian distributions (<code>p(i) ~ N(mean[i], stdvar[i])</code>) and that the <em>pose state</em> variables are noise-free, with values given by the respective sensors. Assuming that the <code>stdvar[i]</code> are constant and known, <strong>Update the <code>mean[i]</code> values</strong>.</p>
<h3 id="task-le-penseur"><a name="user-content-task-le-penseur" href="#task-le-penseur" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>Task: <em>Le Penseur</em></h3>
<blockquote>
<p>Implement a functional deliberation layer.</p>
</blockquote>
<h3 id="task-catch-the-ball-fido"><a name="user-content-task-catch-the-ball-fido" href="#task-catch-the-ball-fido" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>Task: Catch the ball, Fido</h3>
<blockquote>
<p>In the &ldquo;Drone in a box&rdquo; scenario a red ball is tossed and the drone must follow it.</p>
</blockquote>
<h3 id="task-state-estimation-and-deliberation-interplay"><a name="user-content-task-state-estimation-and-deliberation-interplay" href="#task-state-estimation-and-deliberation-interplay" class="headeranchor-link" aria-hidden="true"><span class="headeranchor"></span></a>Task: State estimation and Deliberation interplay</h3>
<blockquote>
<p>Assuming that deliberation is based in &ldquo;plans&rdquo;, study their compatability with the world state evolution model used in the state estimation layer. In particular answer the following questions:</p>
<ul>
<li>What does it mean &ldquo;a plan is incompatible with the world model&rdquo;?</li>
<li>What does it mean &ldquo;a plan is entailed by the world model&rdquo;?</li>
<li>Can plans help in the current state estimation? If so, how?</li>
<li>Can the world model help tunning plans?</li>
</ul>
</blockquote></article></body></html>