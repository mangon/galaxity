# Feedback

## Key comments:
- "The results show not so impressive improvements of the proposed solution in the gold miners problem."
- "I consider that the paper should **clarify better its main proposal**, [...] it is not evident what is the main proposition, and most importantly, **how does it generalize to other problems** than the gold miners."
- "I think it would be important to provide a **better state of the art section**, **separate the main proposal from the example application**, and if possible **enrich the evaluation of the proposal**."
- "the paper would benefit from a more **formal presentation of some of the theory**."
- "I also found the presentation of results extremely brief and think your **discussion of related work could usefully be expanded**."
- "I think **more information on background information** would be useful in section 2."
- "My key requirement for acceptance of this paper would be a **proper theoretical discussion** about the features of percept behaviour that are required for it to apply, and an **explicit and formal description divorced from the case study** which explains how probabilistic reasoning is used to select a predicate from a set for sending to the agent"
- "A section **explicitly discussing further work** would be good, especially since you emphasise the preliminary nature of that in the paper."
- "I suggest you take a look at a number of application areas in hybrid systems - e.g., UAVs [...] and come up with a stronger conclusion about further case studies that could be performed in this area."

## Reviewer #1: RATINGS OF PAPER

- 0: Neutral (I don't like it, but I won't object if others like it)
- Originality of the paper: Not so original but useful proposal
- Technical soundness: Weak in state of the art and evaluation
- Significance: Useful and interesting approach
- Clarity of presentation: The paper needs reorganisation
- Relevance to KAIS:
- Is the paper topic on the KAIS topic list?  (x ) yes ( ) no
- LENGTH (relative to the useful contents of the paper) Should be extended: as mentioned in the recommendations

### OVERALL RECOMMENDATION
Author should prepare a major revision for a second review:
If the paper is accepted, it should be published as a short paper:

### A SHORT SUMMARY OF THE RATIONALE FOR YOUR RECOMMENDATION

The paper presents an interesting version of the gold miners problem discussed in the agent programming literature, however it gives the reader an expectation of a more general proposal for the problem of probabilistic perception.

### DETAILED COMMENTS FOR AUTHOR(S)

The paper tackles interesting aspects of the integration of logic and probabilistic AI in a practical problem, bringing an alternative solution for probabilistic perception for logic-based agent.

The paper presents a nice motivation to the problem, showing the impact of noisy perception for a logic agent in a well known scenario (the gold miners).

Section 2 (state of the art) is actually more background than state of the art in the problem of probabilistic perception in logic based agents.
Section 2.2 discusses the adaptation of HMM and DBN to agent related problems.

Section 3 presents the paper main proposal - the probabilistic perception correction. Some related work is discussed in this section, perhaps that should be in the previous section (state of the art).

Then the example problem (gold miner) is developed according to the proposal. In the discussion of the example the paper seems to go back to its central idea and explains better the architecture of the proposal.

The results show not so impressive improvements of the proposed solution in the gold miners problem.

I consider that the paper should clarify better its main proposal, for instance, Figure 5, which is in the example section, could come first. For the same reason it is not evident what is the main proposition, and most importantly, how does it generalize to other problems than the gold miners.

I think it would be important to provide a better state of the art section, separate the main proposal from the example application, and if possible enrich the evaluation of the proposal.

Please, numerate all equations.


## Reviewer #2: RATINGS OF PAPER

[Please rate the following by entering a score between -3 to 3 with 0 being the average based on the following guidelines:

 	3: Strong Accept (As good as any top paper in reputable journals)
 	2: Accept (Comparable to good papers in reputable journals)
 	1: Weak Accept (I vote acceptance, but won't argue for it)
 	0: Neutral (I don't like it, but I won't object if others like it)
	-1: Weak Reject (I would rather not see this paper accepted)
	-2: Reject (I would argue to reject this paper)
	-3: Strong Reject (Definitely detrimental to the journal quality if
    	accepted)]

- Originality of the paper: 1
- Technical soundness: 1
- Significance: 0
- Clarity of presentation: 1
- Relevance to KAIS: 2
- Is the paper topic on the KAIS topic list?  (x ) yes ( ) no
- LENGTH (relative to the useful contents of the paper) Should be extended:

### OVERALL RECOMMENDATION

Author should prepare a major revision for a second review: If the paper is accepted, it should be published as a regular paper:

### A SHORT SUMMARY OF THE RATIONALE FOR YOUR RECOMMENDATION

This appears to be a promising paper in an interesting area.  It is lacking the the technical details necessary to present itself as a fully general framework - the translation from probabilistic reasoning to beliefs is explained informally and via the example - and it would benefit from the inclusion of some worked examples, and fuller presentation of the case study results.  The authors admit that it is very preliminary work and seem rather vague about future directions and possible applications.  It could also use a proof-read by a native English speaker.


### DETAILED COMMENTS FOR AUTHOR(S)

The inclusion of probabilistic reasoning into BDI programming is an important area and it is nice to see a system that comes with a working implementation.  However while the implementation is impressive the paper would benefit from a more formal presentation of some of the theory.  I also found the presentation of results extremely brief and think your discussion of related work could usefully be expanded.

In detail.

You mention related work in brief at the top of page 2, but don't really provide any insight how your own work differs from or improves this other work.  For a journal paper, I would recommend a separate section on related work in which you discuss this more fully.

I think more information on background information would be useful in section 2.  For instance it wouldn't hurt to provide an example of a Jason plan and discuss the part beliefs play in its application and execution and an example of where it causes an action to be performed in the environment.  I think you may also want to discuss the way actions and perception do interleave in Jason.  I assume your use of the "skip" action is because Jason can get percepts twice without any action being executed in the environment in between, but you should make this clearer. You should also discuss, at least briefly, what happens when there is a delay between Jason executing an action and its outcomes becoming perceptible.

Similarly a little more detail in the presentation of HMMs wouldn't go amiss, such as elaborating the meaning of the P( X | Y) notation and the distinction between values x, y, values x, y with ~ over them and values x, y with ^ over them.

In section 3.1 I didn't really follow the discussion of the different versions of the Goldminers simulation, nor did I grasp which version was used to generate your experimental results.

I think it would be useful to separate out some of the discussion in section 3.2 into a separate subsection of section 2.  For instance it seems to me fairly crucial to your existing framework that percepts (or some subset of possible percepts) are drawn from a finite set (e.g., the states that a square can be in) and that the elements of that finite set are mutually exclusive.  Therefore you are using probabilistic techniques to select a single predicate from that finite set which is then forwarded to the agent.  None of this is made explicit in your description and seems to be a key theoretical part of your methodology that must be formally presented and discussed.   My key requirement for acceptance of this paper would be a proper theoretical discussion about the features of percept behaviour that are required for it to apply, and an explicit and formal description divorced from the case study which explains how probabilistic reasoning is used to select a predicate from a
set for sending to the agent.

Equations 9 & 10 and several following use a very non-standard notation where quantifiers appear after the expression that contains the bound variables.  I'm not sure if this is standard in probabilistic reasoning, but it looks very strange to someone coming from a logic background.

I think section 3 would also benefit from a worked example at the end which shows how particular readings from some sensors eventually get converted into a predicate that is sent to the agent.

Do you have any values for the significance of the improvements you present in section 4.  Given this is a journal paper I think there is also scope to present the raw data you have aggregated to produce the graph.  I think it would also be useful if you could give the actual execution times, even though you claim there was negligible difference in timings between the two versions of the system.  In the spirit of Open Science I would also encourage you to make your code available somewhere so other researchers can re-run your simulations (ideally I would encourage archiving your system in an executable form somewhere like recomputation.org, Elsevier's SHARE system, or a Springer equivalent if such is available) and include a mention in your paper about where it can be found.

A section explicitly discussing further work would be good, especially since you emphasise the preliminary nature of that in the paper.

Your final paragraph is also very weak "it is hard .... to foresee... applications".  I suggest you take a look at a number of application areas in hybrid systems - e.g., UAVs often make use of redundant sensors and perform polling across sensor inputs as well as combining data from infra-red and radar to make judgements and come up with a stronger conclusion about further case studies that could be performed in this area.

### Grammatical Errors and Typos (this is only a subset)

Page 1, line 19.  benefits -> benefit

Page 1, line 28. Probabilist -> Probabilistic

Page 1, line 34.  What do you mean by delve?

Page 1, line 35.  What do you mean by vocation?

Page 2, line 14.  I very much doubt you mean seminal here.

Page 2, line 25.  "a small amount of sensor misreadings is" -> "a small number of sensor misreadings are"

Page 2, line 37.  "in the perception" -> "on perception"

Page 2, line 38.  "differ of" -> "differ from"

Page 6, line 13.  Do you mean Binding rather than Bounding?

Page 6, line 29.  "if great contribute" -> "to make a great contribution"
