%!TEX root = ./PBRFASL.tex
%
%
%
\section{State of the Art}\label{section:state_of_the_art}
%
%
%	Suggestions from the reviewers:
%
%		- **a better state of the art section**
%		- **a more formal presentation of some of the theory**
%		- **more information on background information**
%

In this section we outline the relevant aspects of the subjects that are the groundwork of our proposal, namely the \acl{ASL} language, its interpreter and framework \JASON, together with \acfp{PGM}, \acfp{DBN}, and their simplification, \acfp{HMM}.


\subsection{\acl{ASL} and \JASON}

\begin{figure*}[t]
	\begin{center}
		%
		\footnotesize
		\begin{tikzpicture}[]
			\node at (0,2) (ENV1) {Percepts};

			\node[draw,fill=gray!50!white] at (1.5,2) (BRF) {BRF};

			\node[draw, rounded corners] at (3.5,3) (BB) {Beliefs};
			\node[draw, rounded corners] at (3.5,1) (EV) {Events};
			\node[draw, rounded corners] at (3.5,2) (PL) {Plans Library};

			\node[draw] at (6,3) (GO) {Generate Options};
			\node[draw] at (6,1) (SE) {\Sel{E}};

			\node[draw] at (8,3) (SO) {\Sel{O}};
			\node[draw, rounded corners] at (8,2) (IQ) {Intentions};
			\node[draw] at (8,1) (SI) {\Sel{I}};

			\node at (10,2) (ENV2) {Action};
			\path[->, every edge/.style={cond}]
				(ENV1) edge (BRF)
			%	(BRF) edge (BB)
			%	(BB) edge (BRF)
			%	(BRF) edge (EV)
				(EV) edge (SE)
			%	(PL) edge (GO)
				(SE) edge (GO)
				(BB) edge (GO)
				(GO) edge (SO)
				(SO) edge (IQ)
				(IQ) edge (SI)
			%	(SI) edge (ENV2)
			;
			\draw[cond,->,rounded corners=3pt] (PL) -| (GO);
			\draw[cond,->,rounded corners=3pt] (BRF) |- (EV);
			\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
			\draw[cond,->,rounded corners=3pt] (SI) -| (ENV2);
		\end{tikzpicture}
	\end{center}
	%
	\caption{The \JASON\ deliberation process very resumed, with the \acf{BRF} highlighted. Percepts from the environment are processed by the \ac{BRF} to generate events and update the beliefs base. The available options are instantiated plans triggered by one event (selected by $\Sel{E}$) and compatible with the current beliefs. One option (defined by $\Sel{O}$) is then appended to the intentions set where $\Sel{I}$ chooses an action.}
	%
	\label{fig:jason.deliberation}
\end{figure*}{}
%

\acf{BDI} is the predominant architecture used for defining intelligent agents.  
\acf{ASL}~\cite{rao1996agentspeak} can be described as a logic programming based language geared towards the \ac{BDI} architecture.  \JASON~\cite{bordini2007programming,bordini2006bdi} implements the operational semantics of an extension of \ac{ASL} and its deliberation cycle is depicted in Figure \ref{fig:jason.deliberation}.  In this cycle, the environment generates percepts that are processed by a \acf{BRF}. Each change in the beliefs base generates an event (in this case an external event as opposed to the internal ones that result from intentions). Goals in the set of events represent different desires that the agent can select. That selection is performed by the function $\Sel{E}$. The selected event defines a set of applicable plans (options) instantiated from the plans library.  The selection of a plan between the applicable ones is performed by the function $\Sel{O}$ and included in the set of (current) intentions. Finally function $\Sel{I}$ selects from the set of intentions the one (action) that is going to be performed by the agent.

Although the \ac{BRF} evaluation is not part of the \ac{ASL} specification it is a necessary component of the architecture. The default one that comes with \JASON\ ``simply updates the belief base and generates the external events in accordance with current percepts. In particular, it does not guarantee belief consistency.''~\cite{bordini2006bdi} (nevertheless, in~\cite{Alechina:2006:BRA:1160633.1160868}, the authors present a polynomial-time belief revision algorithm that restores belief base consistency when there are derived inconsistencies).
%

%
\JASON\ is used as the \ac{ASL} framework and scenario simulator in this work.

%
%
%
\subsection{\aclp{HMM} and \aclp{DBN}}\label{subsec:dbn.and.hmm}
%
%	- ground hmm and dbn in pgms
%	- give better descriptions of pgm, hmm and dbn
%
\begin{figure}[t]
	\begin{center}
	\begin{tikzpicture}
		\node[varnode] (X0) at (-6,1) {$x\TS{0}$};
		%
		\node[varnode] (X1) at (-3,1) {$x\TS{1}$};
		\node[detnode] (Y1) at (-3,0) {$z\TS{1}$};
		\node[detnode] (A1) at (-3,2) {$u\TS{1}$};
		%
		\node[varnode] (X) at (0,1) {$x\TS{t-1}$};
		\node[detnode] (Y) at (0,0) {$z\TS{t-1}$};
		\node[detnode] (A) at (0,2) {$u\TS{t-1}$};
		%
		\node[varnode] (X') at (3,1) {$x\TS{t}$};
		\node[detnode] (Y') at (3,0) {$z\TS{t}$};
		\node[detnode] (A') at (3,2) {$u\TS{t}$};
		\path[->]
			%		
			(X0) edge (X1)
			%
			(A1) edge (X1)
			(X1) edge (Y1)
			%
			(X1) edge[dotted] (X)
			%
			(A) edge (X)
			(X) edge (Y)
			%
			(X) edge (X')
			%
			(A') edge (X')
			(X') edge (Y')
		;
	\end{tikzpicture}	
	\end{center}
	\caption{The \ac{HMM} filtering problem extended with actions: From a stochastic process $x\TS{0:\ldots}$ are known a controlled \emph{transition model}, $\PR{X' \GIVEN X, U}$, and an \emph{observation model}, $\PR{Z \GIVEN X}$. Estimate $x\TS{t}$ given a previous estimate $\hat{x}\TS{t-1}$ of $x\TS{t-1}$, an action $u\TS{t}$ and an observation $y\TS{t}$. This problem can be solved, for example, by the \emph{forward algorithm}.}\label{fig:hmm.setting}
\end{figure}


\acf{HMM} is a well-known framework for the estimation of latent variables in stochastic processes \cite{barber2012bayesian}. The standard setting for the \emph{filtering} problem is as follows: A  (discrete) system state at time step $t$ is described by a random variable $X\TS{t}$ (we denote the time-step as a parenthesized exponent, linear ranges $i, \ldots, j$ as $i:j$, random variables by uppercase letters and the respective values by lowercase) that verifies the Markov condition:
\begin{align}
	\forall t \geq 0\quad \PR{ X\TS{t+1} \GIVEN X\TS{0:t} } &= \PR{X\TS{t+1} \GIVEN X\TS{t}}
\end{align}
or, as an independence statement,
\begin{align}
	\forall t\geq 0\quad \INDG{X\TS{t+1}}{X\TS{0:t-1}}{X\TS{t}}.
\end{align}
The conditional $\PR{X' \GIVEN X}$ is the \emph{transition model} of the system. Now suppose that $X$ is hidden but stochastically measurable, \emph{i.e.} a \emph{sensor model} $\PR{Z \GIVEN X}$ is known and values $z\TS{t}$ observed. The \emph{filter problem} is to estimate $x\TS{t}$ given an \emph{initial state}, $x\TS{0}$, and a \emph{sequence of observations}, $z\TS{1:t}$:
\begin{align}
	\forall t > 0\quad \hat{x}\TS{t} \sim \PR{X \GIVEN x\TS{0}, z\TS{1:t}}.
\end{align}

The common (and very beautiful) \emph{forward} algorithm for the exact estimation of $\hat{x}\TS{t}$ has two steps: First, given a \emph{belief} about the previous system state, $\hat{x}\TS{t-1}$, \emph{forward an expectation} of the current state, $\bar{x}\TS{t}$, using the transition model, $\PR{X' \GIVEN X }$:
\begin{align}
	\forall t > 0\quad \bar{x}\TS{t} &\sim \PR{X' \GIVEN X = \hat{x}\TS{t-1}}\label{eq:estimate.forward}
\end{align}

Second, given the current observation, $z\TS{t}$, \emph{correct the forward expectation} using the sensor model, $\PR{Z \GIVEN X}$, and Bayes' law
\begin{align}
	\forall t > 0\quad \hat{x}\TS{t} \sim \PR{X \GIVEN Z = z\TS{t} } &\propto
		\PR{ Z = z\TS{t} \GIVEN X = \bar{x}\TS{t} }
		\PR{ X = \bar{x}\TS{t} }\label{eq:estimate.correction}
\end{align}

Therefore it is only necessary to store the belief about the previous environment state, $\hat{x}\TS{t-1}$, and update it with the current sensor reading, $y\TS{t}$. The major problem with a na\"{i}ve approach of \acp{HMM} is that the size of the transition model is quadratic in the number of system states. \acfp{DBN} provide a more sophisticated approach that tries to minimize this problem by exploiting independences in the \emph{structure} of the system and, hopefully, producing sparse representations of the transition and observation models \cite{murphy2002dynamic,murphy2012machine}.

The general \ac{HMM} and \ac{DBN} framework can be used directly to describe agent related problems. Agent perceptions are already represented by the observation model and actions can be represented by a random (but observed) variable, say $A\TS{t}$. The transition model becomes $\PR{X\TS{t} \GIVEN X\TS{t-1}, A\TS{t}}$ and the filter problem is extended with a sequence of actions:
\begin{align}
	\PR{X\TS{t} \GIVEN X\TS{0}, y\TS{1:t}, a\TS{1:t}}.
\end{align}
The expectation step in the forward algorithm now takes the current action into account
\begin{align}
	\PR{\tilde{X}\TS{t}} &= \PR{X\TS{t} \GIVEN X\TS{t-1} = \hat{x}\TS{t-1}, A\TS{t} = a\TS{t}}
\end{align}
but the correction step remains unchanged.
%

%

\ac{DBN} are used in this work to correct agent perceptions. In this specific case the transition and sensor models have many independence relations that are exploited to produce sparse matrices. The next section describe the construction of such models.