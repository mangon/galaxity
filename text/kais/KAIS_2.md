# KAIS 2

1. Introdução e motivação
2. Estado da arte
	1. Background
	2. Bleeding edge
3. Theoretical proposal
	1. BDI vbn
	2. DBN fc
	3. OUR vbn
4. Application
	1. Gold miners
	2. Others
5. Related work
	1. Comparison with similar work
6. Conclusions and Future work
	1. Future Work: Problog (vbn), UAVs (fc)