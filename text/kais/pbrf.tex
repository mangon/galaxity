%!TEX root = ./PBRFASL.tex
\section{Probabilistic Perception Correction}
\label{sec:probabilistic.perception.correction}

The task we address in this section is to illustrate how probabilistic methods can be used to improve the performance degradation in the \GM\ noisy scenario without changing the symbolic processing system as originally used by the winning team \cite{bordini2006bdi}. 

Currently the problem of extending \ac{ASL} data structures with probabilistic features is mostly directed to belief representation and being addressed by different authors \cite{luzalternatives,fagundes2009deliberation,fagundes2007integrating,kieling2011insertion,silva2011agentspeak} but isn't yet fully solved.
%
An alternative and less intrusive application of probabilistic \ac{AI} to \ac{ASL} targets the processes instead of the data structures.
%

%
Bounding probabilistic techniques to the computation of certain \ac{ASL} functions (the \ac{BRF}, selections of events, options and intentions\footnote{Recent versions of \JASON\ include an inbox, an outbox and a select message function.}, \emph{etc.} as in figure \ref{fig:jason.deliberation}) promises a number of advantages.
%
Since the computations of those functions are unspecified in \ac{ASL} (and over writable in \JASON), probabilistic techniques can be used there without invalidating previous work.
%
So symbolic and probabilistic \ac{AI} have clearly separated roles and each is used to solve ``familiar'' problems in the respective domain, rich in mature techniques and tools, while both simultaneously contribute to the agent behavior: 
		\begin{itemize}
			\item symbolic programming uses unchanged \ac{ASL} programs to define high level agent behavior, with plans, goals, beliefs, resolution, \emph{etc};
			\item probabilistic algorithms can use unchanged tools (\emph{e.g.} \WEKA\   \cite{bouckaert2010weka,witten2005data,dimov2007weka,hall2009weka}, \SAMIAM\footnote{From \url{http://reasoning.cs.ucla.edu/samiam/}}, \OPENMARKOV\footnote{From \url{http://www.openmarkov.org}} or \DLIB\footnote{From \url{http://dlib.net}.})  to process low level noisy signals --- with \acp{BN}, influence diagrams, monte-carlo Markov chains, expectation-maximization, \emph{etc};
		\end{itemize}	
	

%
In summary, defining the computation of certain functions of \ac{ASL} as tasks to be solved by probabilistic techniques seems to pose a promising technique to address open problems in agent autonomy, with the potential of great contribute for an evolution (and not a revolution) of agent programming.
%

%
Next we describe the setup of an experiment where this interplay of symbolic and probabilistic techniques is used to illustrate such technique. This experiment
\begin{enumerate}
	\item uses original \ac{ASL} \JASON\ programs designed for a (mostly) deterministic scenario;
	\item adds sensor misreadings to that scenario, the respective rate being defined by a parameter;
	\item re-defines the computation of the \ac{BRF} with the help of a probabilistic process to correct perceptions;
	\item and records the performance of agents using this re-defined \ac{BRF}.
\end{enumerate}
The outcome of the first two steps is already depicted in Figure \ref{fig:experiment1.results} where performance degradation is clearly associated with higher rates of sensor misreadings.
%

%
The next subsections detail the original deterministic scenario, changes made to add sensor misreadings, the probabilistic correction of perceptions and the construction of the transition model. Results and further discussion follows in the next sections.
%

%
\subsection{Problem Statement: (Noisy) \GM}
%
\begin{figure}[t]
	\begin{center}
		\begin{tabular}{lr}	
		\begin{tikzpicture}
			\draw (0,0) grid (5,5);
			\draw[fill=black!25] (2,2) rectangle (3,3);
			\begin{scope}[shift = {(1.5, 1.5)}]
				\sensorgrid
			\end{scope}
		\end{tikzpicture}
		&	
		\begin{tikzpicture}
			\draw (0,0) grid (5,5);
			\draw[fill=black!25] (2,3) rectangle (3,4);
			\begin{scope}[shift = {(1.5, 2.5)}]
				\sensorgrid
			\end{scope}
		\end{tikzpicture}
		\\
		Before moving up. & After moving up.
			
		\end{tabular}
	\end{center}
	\caption{Sensor grid. The miner is in the location scanned by $Y_4$ where it can pick a gold (if present) or drop (if empty). Values reported by the sensors depend on the noise parameter and the contents of the cell in the sensor location. As the miner moves his sensors occupy different cells in the environment.}\label{fig:sensor.grid}
\end{figure}
%
%

The original environment of the \GM\ competition is partially observable, stochastic, sequential, dynamic and discrete (see \cite{russell2003artificial} about this classification). The competition description (in  \cite{dastani2007second}) states that ``[t]he environment of the multi-agent system was a grid-like world where agents could move from one cell to a neighboring cell if there was no agent or obstacle already in that cell. In this environment, gold could appear in the cells. Participating agent teams were expected to explore the environment, avoid obstacles and compete with another agent team for the gold. The agents of each team could coordinate their actions in order to collect as much gold as they could and to deliver it to the depot where the gold can be safely stored. Agents had only a local view on their environment, their perceptions could be incomplete, and their actions could fail.''
%

%
Although non-determinism is already present in the original competition scenario in the form of incomplete perception and action failure the \GM\ examples in the \JASON\ distribution (besides providing a proxy to that competition simulator) optionally use a local simulator for development and evaluation purposes. In that local simulator noise only increases the probability of action failure in proportion to current cargo. Thus environments in the local simulator change from stochastic to strategic (because gold no longer appears randomly in cells and the only changes in the environment state are produced by agents).
%

%
The \GM\ miners team follows a coordination protocol concerning the collection and transport of newly found gold pieces. There are two kinds of agents, a single leader and a set of miners. 
%
``[The] leader helps the miners to coordinate themselves in [\ldots] the negotiation process that is started when a miner sees a piece of gold and is not able to collect it (because its container is full). This miner broadcasts the gold location to other miners who then send bids to the leader. The leader chooses the best offer and allocate the corresponding agent to collect that piece of gold [\emph{in} \cite{hubner2008developing,bordini2006bdi}].''
%

%
A (robotic) miner is equipped with a $3\times 3$ grid of sensors, $Y_{0:8}$, (Figure \ref{fig:sensor.grid}) that scans its neighborhood. Each sensor scans the contents of a cell at a certain location and reports the content, that can be one of \emph{empty}, \emph{obstacle}, \emph{gold} or \emph{miner}. The miner can also select an action to transform his immediate neighborhood. Action options are \emph{up}, \emph{down}, \emph{left}, \emph{right}, \emph{pick}, \emph{drop} and \emph{skip}.
%

%
Values depicted in Figure \ref{fig:experiment1.results} result from this local simulator with noisy sensors. The ``noise'' parameter is the rate of cell contents misreadings by a sensor. When this parameter is zero simulations are noise free. Sensors are independent but equally parameterized: the value reported by each sensor depends only on the noise parameter (the same for all sensors) and cell content.
%

%

\subsection{Proposal: Percept-Correction Function}
%
\begin{figure}[t]
	\begin{center}	
		\begin{tabular}{c}
			Detail current belief revision\ldots
			\\
			\begin{tikzpicture}						
				\node at (-1,2) (ENV1) {Percepts};
				\node[draw, fill=black!5] at (1,2) (BRF) {\ac{BRF}};
				\node[draw, rounded corners] at (3,3) (BB) {Beliefs};
				\draw[cond, ->] (ENV1) -> (BRF);
				\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
			\end{tikzpicture}
			\\
			\\		
			\ldots to include perception correction before symbolic \ac{BRF}.
			\\
			\begin{tikzpicture}		
				\node at (-1,2) (ENV1) {Percepts};
				\node[draw, fill = white] at (1,2) (PCF) {\acs{PCF}};
				\node[draw, rounded corners] at (-1,3) (SB) {Sensors Belief};
				\node[draw, fill = white] at (3,2) (BRF) {s\ac{BRF}};
				\node[draw, rounded corners] at (5,3) (BB) {Beliefs};
				\draw[cond, ->](ENV1) edge (PCF);
				\draw[cond, ->] (PCF) edge (BRF);
				\draw[cond,<->,rounded corners=3pt] (BRF) |- (BB);
				\draw[cond,<->,rounded corners=3pt] (PCF) |- (SB);
				\begin{pgfonlayer}{background}				
					\draw[fill=black!5]
						($(PCF.north west)+(-0.25,-0.75)$)
						rectangle
						($(BRF.south east)+(0.25,0.85)$);	
				\end{pgfonlayer}
			\end{tikzpicture}
		\end{tabular}
	\end{center}
	\caption{Inclusion of \acf{PCF} before the original, symbolic, \ac{BRF} (denoted by s\ac{BRF}) to correct noisy perceptions. The Sensors Belief is a distribution of sensor values and independent of the (symbolic) beliefs used in the \ac{BDI} deliberation.}\label{fig:include.pcr}
\end{figure}
%
%
%
Our proposal to recover agent performance is to enter the \ac{BRF} step of \ac{BDI} and prepends a probabilistic \acf{PCF} step to take advantage of probabilistic knowledge of the environment dynamics (see Figure \ref{fig:include.pcr}).
%

%
Correction of perceptions can be stated as an inference problem, a natural framework to deal with sensor uncertainty. \acfp{PGM} and, in particular, \acp{HMM} and \acp{DBN} (which are \acp{HMM} exploiting problem structure) provide such framework. 
%

%
\bigskip
\textbf{Formal problem statement.} \emph{In the setting of the \GM\ simulation extended with sensor noise parameter $\theta$,  let $X\TS{t}_{i}$ denote the (hidden) content of the cell scanned by sensor $i$ (at time $t$), $Y\TS{t}_i$ the observed value and $A\TS{t}$ the action executed, according to figure \ref{fig:hmm.setting}. Update the estimate of cells content $\hat{x}\TS{t}_{0:8}$ given the previous estimate $\hat{x}\TS{t-1}_{0:8}$, current action $A\TS{t} = a$ and sensor readings $Y\TS{t}_{0:8} = y\TS{t}_{0:8}$. For notation simplicity we write $X\TS{t} = X\TS{t}_{0:8}, Y\TS{t} = Y\TS{t}_{0:8}$ \emph{etc}.}
%

\bigskip
%
The \ac{HMM} and \ac{DBN} resolution of this problem is organized as follows. Given the previous cell content estimate $X = X\TS{t-1} = \hat{x} = \hat{x}_{0:8}$, current action $A' = A\TS{t} = a'$ and sensor reading $Y' = Y\TS{t} = y' = y'_{0:8}$, the estimate update, $\PR{X'} = \PR{X\TS{t}}$, is given by
\begin{align}
	\PR{X' \GIVEN\ X = \hat{x}, A' = a', Y' = y'} & \propto \PR{Y' = y' \GIVEN X'}\PR{X' \GIVEN X = \hat{x}, A' = a'}\label{eq:estimate.update}
\end{align}
and perception correction is the \ac{MAP} of each sensor
\begin{align}
	\hat{x}'_i &= \arg_{x} \max \PR{X'_i = x \GIVEN Y' = y', A' = a', X = \hat{x}}\label{eq:MAP.perception}
\end{align}
where $x$ ranges over all (four) sensor values. The $\arg_{x}\max$ computation  in Equation \ref{eq:MAP.perception} doesn't require normalizing the right side of Equation \ref{eq:estimate.update}, which is a welcome simplification.

Direct calculation of Equation \ref{eq:estimate.update} doesn't scale to the grid sensor. Since each location has four different values the state space has $4^9 = 262144$ different values. Then for each action $A = a$ the corresponding transition has $262144^2 = 68719476736$ parameters. Numbers of this magnitude render the representation and computations intractable by a direct \ac{HMM} approach and this problem is worsened by considering that this is the first step of the ``real-time'' deliberation process.

The grid sensor entails many independence relations (sketched in Figure \ref{fig:grid.independences}) that reduce the number of transition and observation parameters to a convenient size:
\begin{enumerate}
	\item Sensor values are independent between them
	\begin{align}
		\PR{X' \GIVEN\ X, A'} &= \prod_{i=0}^8\PR{X'_i \GIVEN\ X, A'}\label{eq:intersensor.independence}
	\end{align}
	so that instead of considering one ``big'' transition, $\PR{X' \GIVEN X = \hat{x}, A' = a'}$, we only need to deal with small transitions, $\PR{X'_i \GIVEN\ X, A'}, i = 0:8$;
	\item Updated values of each sensor depend only on the action and the previous values of neighbor sensors
	\begin{align}
		\PR{X'_i \GIVEN\ X, A'}
			&= \PR{X'_i \GIVEN\ X_{N\at{i}}, A'}\label{eq:sensor.neighbours}
	\end{align}
	where $N\at{i}$ is the set of neighbors of sensor $i$, defined in Table \ref{table:grid.neighbours}. This reduction in the dependencies can be further refined when the action is considered (for example for the ``up'' action the ``bottom'' neighbors are irrelevant)
	\begin{align}
		\PR{X'_i \GIVEN\ X, A' = a'}
			&= \PR{X'_i \GIVEN\ X_{N_{a'}\at{i}}, A' = a'}\label{eq:sensor.neighbours.and.action}
	\end{align}
	where $N_{a'}\at{i}$ is the set of relevant neighbors for action $A'=a'$;
	\item The observed value of a sensor depends only on the corresponding cell content:
	\begin{align}
		\PR{Y'_i \GIVEN\ X'} &= \PR{Y'_i \GIVEN\ X'_i}\label{eq:observation.independence}
	\end{align}
\end{enumerate}
%
%
%
\begin{table}[t]
\caption{Neighbors in the grid sensor.}\label{table:grid.neighbours}
\begin{align}
	N\at{0} &= \SET{0,1,3} & N\at{1} &= \SET{0,1,2,4} & N\at{2} &= \SET{1,2,5} \\
	N\at{3} &= \SET{0,3,4,6} & N\at{4} &= \SET{1,3,4,5,7} & N\at{5} &= \SET{2,4,5,8} \\
	N\at{6} &= \SET{3,6,7} & N\at{7} &= \SET{4,6,7,8} & N\at{8} &= \SET{5,7,8}
\end{align}
\end{table}
%
\begin{figure}[t]
	\begin{center}
		\begin{tikzpicture}
			\node[var] (X0) at (0,2) {$X_0$};
			\node[var] (X1) at (1,2) {$X_1$};
			\node[var] (X2) at (2,2) {$X_2$};
			\node[var,gray!50] (X3) at (0,1) {$X_3$};
			\node[var] (X4) at (1,1) {$X_4$};
			\node[var,gray!50] (X5) at (2,1) {$X_5$};
			\node[var,gray!50] (X6) at (0,0) {$X_6$};
			\node[var,gray!50] (X7) at (1,0) {$X_7$};
			\node[var,gray!50] (X8) at (2,0) {$X_8$};
			%
			\node[var,gray!50] (X'0) at (4,2) {$X'_0$};
			\node[var] (X'1) at (5,2) {$X'_1$};
			\node[var,gray!50] (X'2) at (6,2) {$X'_2$};
			\node[var,gray!50] (X'3) at (4,1) {$X'_3$};
			\node[var,gray!50] (X'4) at (5,1) {$X'_4$};
			\node[var,gray!50] (X'5) at (6,1) {$X'_5$};
			\node[var,gray!50] (X'6) at (4,0) {$X'_6$};
			\node[var,gray!50] (X'7) at (5,0) {$X'_7$};
			\node[var,gray!50] (X'8) at (6,0) {$X'_8$};
			%
			\node[var,gray!50] (Y0) at (8,2) {$Y'_0$};
			\node[var] (Y1) at (9,2) {$Y'_1$};
			\node[var,gray!50] (Y2) at (10,2) {$Y'_2$};
			\node[var,gray!50] (Y3) at (8,1) {$Y'_3$};
			\node[var,gray!50] (Y4) at (9,1) {$Y'_4$};
			\node[var,gray!50] (Y5) at (10,1) {$Y'_5$};
			\node[var,gray!50] (Y6) at (8,0) {$Y'_6$};
			\node[var,gray!50] (Y7) at (9,0) {$Y'_7$};
			\node[var,gray!50] (Y8) at (10,0) {$Y'_8$};
			%
			\node[var] (A) at (5,3.5) {$A'$};
			%
			\path[->]
				(X0) edge[bend left] (X'1)
				(X1) edge[bend left] (X'1)
				(X2) edge[bend right] (X'1)
				(X4) edge[bend right] (X'1)
				(A) edge (X'1)
				(X'1) edge[bend left] (Y1)
			;
		\end{tikzpicture}
	\end{center}
	\caption{Parent relations in the grid for sensor \#1. The edges represent the chain of dependency for $Y'_1$. Other sensors in the grid have similar parent sub-networks. Notice that the parents of $X'_i$ are $X_{N\at{i}}$ and $A'$.}\label{fig:grid.independences}
\end{figure}
%

%
Using the independence relations of Equations \ref{eq:intersensor.independence}, \ref{eq:sensor.neighbours.and.action} and \ref{eq:observation.independence} the transition and observation models can be described by relatively small matrices and the computations of Equations \ref{eq:estimate.update} and \ref{eq:MAP.perception} become acceptable for the inclusion of the \ac{PCF} in the \ac{BDI} deliberation process.
%

%
The sensor model is very simple: if $\theta$ is the noise parameter then
\begin{align}
	\PR{Y'_i = x \GIVEN X'_i = x} &= 1 - \theta,\:\forall x\label{eq:sensor.model.equal}\\
	\PR{Y'_i = y \GIVEN X'_i = x} &= \frac{1}{3}\theta,\:\forall x, y \not= x\label{eq:sensor.model.different}
\end{align}
The $1/3$ coefficient in Equation \ref{eq:sensor.model.different} covers the three cases where $Y'_i$ can be different from $X'_i$.
%

%
The transition model, more complex than the sensor model, is explained in the next sub-section.
%

%
\subsection{Resolution: Transition Model}\label{subsec:transition.model}
%

%
The construction of the (probabilistic) transition model is based on a few simplifying assumptions about the environment:
\begin{enumerate}
	\item The state of the environment only changes by effect of the miner's actions:
		\begin{itemize}
			\item Detected miners do not move;
			\item Gold doesn't ``appear'' in empty cells;
		\end{itemize}

	\item The miner never moves to ``obstacle'' or ``miner'' cells;
	\item The content of unscanned cells is uniformly distributed over all possible values;
\end{enumerate}
%

%
The state of a sensor $X'_i$ depends only on the action $A' = a'$ and previous values of the neighbors $X_{N_a'\at{i}}$. Different actions entail different schemes for the transition parameters, easier to describe one action at a time. In the following text transitions not described have probability zero.
%

%
\textbf{Action ``skip''.} In this case the miner doesn't change the environment state. Therefore the sensor readings should be the same as before:
\begin{align}
	\PR{X'_i = x \GIVEN X_i = x, A' = \text{skip}} &= 1,\:\forall x,i\label{eq:tr.skip}
\end{align}
%

%
\textbf{Action ``pick''.} The miner removes a gold from its location, if one exists:
\begin{align}
	\PR{X'_i = x \GIVEN X_i = x, A' = \text{pick}} &= 1, \forall x, i \not= 4\label{eq:tr.pick.1}\\
	\PR{X'_4 = \text{empty} \GIVEN X_4 = \text{gold}, A' = \text{pick}} &= 1\label{eq:tr.pick.2}\\
	\PR{X'_4 = x \GIVEN X_4 = x, A' = \text{pick}} &= 1,\:\forall x \not=\text{gold}\label{eq:tr.pick.3}
\end{align}
%

%
\textbf{Action ``drop''.} The miner adds a gold in its location, if that cell is empty:
\begin{align}
	\PR{X'_i = x \GIVEN X_i = x, A' = \text{drop}} &= 1, \forall x,i \not= 4\label{eq:tr.drop.1}\\
	\PR{X'_4 = \text{gold} \GIVEN X_4 = \text{empty}, A' = \text{drop}} &= 1\label{eq:tr.drop.2}\\
	\PR{X'_4 = x \GIVEN X_4 = x, A' = \text{drop}} &= 1, \forall x\not=\text{empty}\label{eq:tr.drop.3}
\end{align}

The remaining actions (``up'', ``right'', ``down'' and ``left'') move the miner in his environment. Only one of these actions is described since they are symmetric.
%

%
\textbf{Action ``up''.} The miner moves up and the sensors that enter unscanned cells are $B = \SET{0,1,2}$. For these the scanned value is uniformly distributed. Each one of the other sensors scans the cell previously above it:
\begin{align}
	\PR{X'_i = x \GIVEN A' = \text{up}} &= \frac{1}{4},\: \forall x, i \in B\label{eq:tr.up.1}\\
	\PR{X'_i = x \GIVEN X_{i-3} = x, A' = \text{up}} &= 1,\: \forall x,i \not\in B\label{eq:tr.up.2}
\end{align}
%

%
The probabilities in Equations \ref{eq:tr.skip} --\ref{eq:tr.up.2} completely define the transition model for the sensor grid.          That model can be used by the forward algorithm outlined in Subsection \ref{subsec:dbn.and.hmm} and Equations \ref{eq:estimate.update} -- \ref{eq:MAP.perception} to correct perceptions.
%

%
The implementation notes and experimental results of a \ac{PCF} using the transition and observation models described in this section are described in the next one.