﻿# Documento de Trabalho

> Uma extensão da investigação Galaxity para concorrer aos projetos financiados pela FTC.
>
> Dado que atualmente (2 de Dezembro de 2014) há a perspetiva de financiamento enquadrado pelo concurso Projetos de IC&DT em todos os Domínios Científicos 2014 importa fazer uma candidatura de modo a
>
> * Aprender o processo de financiamento
> * Tentar a sorte
>
> Este é um documento de trabalho onde a equipa proponente regista tarefas, calendários e ideias.

## AGITO: Equipes de Agentes Inteligentes para Observações Geológicas /Teams of Intelligent Agents for Geologic Observations

## Formulação da proposta

### Enunciado do problema

* Componente geológica
   - Instrumentos
   - Condicionantes
* Componente "aviónica"
   - autonomia
   - _payload_
   - controlo e precisão
* Componente autonomia
   - visão e outros sensores
   - actualização da crença
   - deliberação
* Componente multi-agente
   - coordenação

### Importância do problema

* Consequências humanas
* Consequências ambientais
* Consequências sociais
* Consequências económicas
* Outros factores dignos de nota

### Formação da equipa

* Engenharia Civil?
* Geociências
   - Pedro Nogueira (UE)
   - Rui Dias (UE)
* Aviónica (Mecatrónica)
   - Mário Rui Melício da Conceição (UE)
* Informática
   - Francisco Coelho (UE)
   - Vitor Nogueira (UE)
   - Vitor Santos Costa (FCUP)
   - ? Ricardo Rocha (FCUP)
   - Salvador Abreu (UE)
   - Pedro Salgueiro (UE)

### Estado da arte

* Tipificação
* Hardware
   - Quadcópteros, aviões, balões, etc
   - Autonomia
   - Payload
   - Manobrabilidade e controlo
   - Motherboards, CPUs, Câmaras, etc
   - Baterias
   - Comunicação
* Algoritmos
   - Cooperação
   - Representação
   - Controlo
   - Inferência
   - Comunicação
   - Integração

### Identificação da oportunidade

Atualmente o comportamento sofisticado de agentes autónomos assenta na programação lógica que proporciona um conjunto de ferramentas adequadas à representação e processamento de planos, objetivos e crenças booleanas.

Por outro lado em cenários físicos, como os delineados nesta proposta, as incertezas inerentes ao problema e à percepção do ambiente colocam sérias dificuldades a este tipo de programação. Os problemas de representação e inferência com incerteza estão no domínio da mineração de dados e, principalmente, dos modelos gráficos.

### Proposta de resolução

* Equipas de agentes (heterógenos?)
   * coordenação e cooperação
* Detecção e Análise
* Navegação local
Tarefas
* Especificação do Hardware
* Especificação do Modelo (das crenças)
* Tratamento de imagem
* Simulação