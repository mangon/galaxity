# Darkense

> Uma extensão da investigação _Galaxity_ para concorrer aos projectos financiados pela [FTC](http://www.fct.pt/).
>
> Dado que actualmente (2 de Dezembro de 2014) há a perspectiva de financiamento enquadrado pelo concurso [Projetos de IC&DT em todos os Domínios Científicos 2014](http://www.fct.pt/apoios/projectos/concursos/2014/index.phtml.pt) importa fazer uma candidatura de modo a
>
>	* Aprender o processo de financiamento
>	* Tentar a sorte
>
> Este é um **documento de trabalho** onde a equipa proponente regista tarefas, calendários e ideias. 

## Equipes de Agentes Inteligentes para Observações Geológicas



## Formulação da proposta

### Importância do problema

* ?

### Formação da equipa

* Engenharia Civil?
	- ?
* Geociências
	- Pedro Nogueira (UE)
	- Rui Dias (UE)
* Aviónica?
	- ?
* Informática
	- Francisco Coelho (UE)
	- Vitor Nogueira (UE)
	- Vitor Santos Costa (FCUP)
	- ? Ricardo Rocha (FCUP)
	- ? Salvador Abreu (UE)


### Estado da arte


### Identificação da oportunidade

Actualmente o comportamento sofisticado de agentes autónomos assenta na **programação lógica** que proporciona um conjunto de ferramentas adequadas à representação e processamento de planos, objectivos e crenças booleanas.

Por outro lado em cenários físicos, como os delineados nesta proposta, as incertezas inerentes ao problema e à percepção do ambiente colocam sérias dificuldades a este tipo de programação. Os problemas de representação e inferência com incerteza estão no domínio da **mineração de dados** e, principalmente, dos  **modelos gráficos**.

### Proposta de resolução

* equipas de agentes (heterógenos?)
	- colocação
	- cooperação
* detecção
* navegação local


