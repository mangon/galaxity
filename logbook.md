# Logbook

## 29 de Janeiro de 2014

### Estado Atual

* A ideia de usar ML para resolver os pontos indefinidos da BDI parece interessante: A experiência com os `GoldMiners` ilustrou a (conhecida) fragilidade da abordagem puramente simbólica;
* Existem várias bibliotecas que implementam BDI e ML. Em particular há bibliotecas `Java` (razoavelmente) mantidas: [`Jason` para agentes BDI](http://jason.sourceforge.net/wp/) e [`openmarkov` para ML](http://www.openmarkov.org);

### Tarefas Futuras

* **Aprofundar** o trabalho atual:
    * Continuar a experiência `GoldMiners` implementando a seleção de intenções computada por um diagrama de influência;
    * Replicar a experiência `GoldMiners` mudando:
        * O diagrama de influência (mais completo, mais simples);
        * O ambiente (Robot "Mordomo", outras competições):
        * A aplicação a outras funções de seleção (Mensagens, Eventos, Opções);
* **Enquadrar** o trabalho atual:
    * Quem está a fazer "coisas parecidas"?
    * Qual o estado atual das disciplinas envolvidas?
    * Quais são os problemas interessantes?
    
* **Alargar** o âmbito do trabalho:
    * Como descrever a semântica e enquadrar num modelo teórico?
    * Como aplicar a casos práticos? (Medicina, Energia, Telecomunicações, Assistentes Pessoais, _etc_)